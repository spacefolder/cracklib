/*
 * The one and only purpose of this test is to see if we can compile
 * our Eigen + OpenMesh hybrid class.
 */

#include <iostream>

#include <CrackTriangleMesh.h>

int main(int argc, char* argv[])
{
  CrackTriangleMesh mesh;

  mesh.debug_set_rectangular_triangle_mesh(9, 9);
  for (int i = 0; i < 4000; ++i)
    {
    if (i % 100 == 0)
      {
      mesh.advance_simulation(0.1);
      }
    else
      {
      mesh.advance_simulation(0.1);
      }
    }

  mesh.advance_simulation(0.001);
  
  std::cout << "Success.\n";
  return 0;
}
