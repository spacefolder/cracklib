#include "CrackSimulationMath.h"
#include <iostream>

int main(int argc, char* argv[])
{
  std::vector<Vector3f> corners(3);
  corners[0] = Vector3f(1, 0, 0);
  corners[1] = Vector3f(0, 5, 0);
  corners[2] = Vector3f(10, 16, 0);

  std::vector<Vector3f> basis;

  make_orthonormal_basis(corners, basis);


  std::cout << "Input vectors: "
            << "(" << corners[0].transpose() << "), "
            << "(" << corners[1].transpose() << "), "
            << "(" << corners[2].transpose() << ")\n";

  std::cout << "Orthonormal basis: "
            << "(" << basis[0].transpose() << "),"
            << "(" << basis[1].transpose() << ")\n";

  std::cout << "Dot product of basis vectors: "
            << basis[0].dot(basis[1])
            << "\n";

  // try a more intuitively interpretable example
  corners[0] = Vector3f(10, 10, 10);
  corners[1] = Vector3f(0, 10, 0);
  corners[2] = Vector3f(10, 0, 0);

  make_orthonormal_basis(corners, basis);

  std::cout << "\n\n";
  std::cout << "Input vectors: "
            << "(" << corners[0].transpose() << "), "
            << "(" << corners[1].transpose() << "), "
            << "(" << corners[2].transpose() << ")\n";

  std::cout << "Orthonormal basis: "
            << "(" << basis[0].transpose() << "),"
            << "(" << basis[1].transpose() << ")\n";

  std::cout << "Dot product of basis vectors: "
            << basis[0].dot(basis[1])
            << "\n";

  // and finally a trivially obvious one
  corners[0] = Vector3f(5, 5, 0);
  corners[1] = Vector3f(0, 5, 0);
  corners[2] = Vector3f(5, 0, 0);

  make_orthonormal_basis(corners, basis);

  std::cout << "\n\n";
  std::cout << "Input vectors: "
            << "(" << corners[0].transpose() << "), "
            << "(" << corners[1].transpose() << "), "
            << "(" << corners[2].transpose() << ")\n";

  std::cout << "Orthonormal basis: "
            << "(" << basis[0].transpose() << "),"
            << "(" << basis[1].transpose() << ")\n";

  std::cout << "Dot product of basis vectors: "
            << basis[0].dot(basis[1])
            << "\n";

  return 0;
}

