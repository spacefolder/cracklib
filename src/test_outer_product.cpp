#include <Eigen/Core>
#include <iostream>

int main(int argc, char* argv[])
{
  Eigen::Vector3f a(1, 2, 3), b(4, 10, 20);

//  std::cout << "Product of a and b: " << a * b << "\n";
  std::cout << "Product of a^T and b: " << a * b.transpose() << "\n";
  return 0;
}
