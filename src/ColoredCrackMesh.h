/*
 * ColoredCrackMesh: CrackTriangleMesh with per-vertex color
 * corresponding to separation eigenvalues
 *
 * The separation eigenvalue is the single number that tells us how
 * close any particular vertex is to failing.  It is stored in a
 * vertex property. This class adds a suffix to advance_simulation
 * that retrieves those values and converts them to per-vertex colors.
 *
 */

#ifndef __ColoredCrackMesh_h
#define __ColoredCrackMesh_h

#include "CrackTriangleMesh.h"

typedef OpenMesh::HPropHandleT<bool> HalfedgeBooleanProperty;

class ColoredCrackMesh : public CrackTriangleMesh
{
public:
  ColoredCrackMesh();
  ColoredCrackMesh(MeshGeometry const& initial_geometry);
  ~ColoredCrackMesh();

  void set_initial_geometry(MeshGeometry const& initial_geometry);
  void advance_simulation(float how_long);
  Vector3f material_vertex_color(VertexHandle const& v) const;

  // Get the number of times a new crack occurred or an existing one
  // was extended in the last iteration
  int num_crack_events() const;
  
  // Walk over all the halfedges in the mesh.  Pull out all the
  // boundaries and return them as something suitable to be used as a
  // line loop.
  void extract_boundary_loops(std::vector<unsigned int>& output);
  
protected:
  HalfedgeBooleanProperty AlreadyTraversed;
  VertexBooleanProperty   IsCrackAnchor;
  int NumCrackEvents;
  std::vector<VertexHandle> CrackAnchorVertices;

  // How often should we clear out dead faces and vertices in the
  // mesh?  Defaults to every 50 iterations.
  int GarbageCollectionInterval;

  void collect_crack_anchor_vertices();
  void compute_vertex_color(VertexHandle const& vertex);
  int extract_boundary_loop(VertexHandle const& starting_vertex,
                            std::vector<unsigned int>& output);
  void garbage_collect_mesh();
  void initialize_already_traversed();
  void initialize_is_crack_anchor();
  void request_mesh_renderable_properties();
  void update_vertex_colors();

  // Augment the superclass implementation so that we can mark crack
  // vertices as they are created.
  VertexHandle duplicate_vertex(VertexHandle const& original);
};

#endif
