#ifndef __MeshGeneration_h
#define __MeshGeneration_h

#include "CrackDataTypes.h"

/** Create a uniform rectangular mesh
 *
 * This will create a rectangular mesh with rows * columns * 2 faces.
 * It will be placed in a rectangle with dimensions (r_width x
 * r_height). The tessellation will look like this:
 *
 *  +----+----+
 *  |\   |\   |
 *  | \  | \  |
 *  |  \ |  \ |
 *  |   \|   \|
 *  +----+-----
 *  |\   |\   |
 *  | \  | \  |
 *  |  \ |  \ |
 *  |   \|   \|
 *  +----+-----
 *
 */

void create_uniform_rectangle_mesh(int rows, int columns, float r_width, float r_height, MeshGeometry& output);

/** Create a uniform rectangular mesh with a friendlier tessellation
 *
 * This will create a rectangular mesh with rows * columns * 2 faces.
 * It will be placed in a rectangle with dimensions (r_width x
 * r_height). The tessellation will look like this:
 *
 *  +----+----+
 *  |   /|\   |
 *  |  / | \  |
 *  | /  |  \ |
 *  |/   |   \|
 *  +----+-----
 *  |\   |   /|
 *  | \  |  / |
 *  |  \ | /  |
 *  |   \|/   |
 *  +----+-----
 *
 */

void create_uniform_rectangle_mesh_diamonds(int rows, int columns, float r_width, float r_height, MeshGeometry& output);

void create_even_random_mesh(float width, float height, int num_vertices, MeshGeometry& output, int buckets_per_side=8);

void test_delaunay_mesh(int width, int height, MeshGeometry& output);


#endif
