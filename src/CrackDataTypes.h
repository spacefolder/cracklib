#ifndef __CrackDataTypes_h
#define __CrackDataTypes_h

#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <Eigen/Core>

#include <algorithm>

typedef Eigen::Matrix3f Matrix3f;
typedef Eigen::Matrix2f Matrix2f;

typedef Eigen::Vector3f Vector3f;
typedef Eigen::Vector2f Vector2f;

typedef OpenMesh::TriMesh_ArrayKernelT<> MeshGeometry;

typedef MeshGeometry::VertexHandle VertexHandle;
typedef MeshGeometry::EdgeHandle EdgeHandle;
typedef MeshGeometry::FaceHandle FaceHandle;
typedef MeshGeometry::HalfedgeHandle HalfedgeHandle;

typedef std::pair<float, Vector2f> EigenPair2;
typedef std::pair<float, Vector3f> EigenPair3;
typedef std::pair<HalfedgeHandle, HalfedgeHandle> HalfedgePair;

typedef Eigen::ArrayXXf DynamicArray;

#endif
