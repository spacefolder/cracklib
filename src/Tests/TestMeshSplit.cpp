#include "CrackTriangleMesh.h"
#include <iostream>

// ----------------------------------------------------------------------

void
print_vertices(MeshGeometry const& m)
{
  for (MeshGeometry::ConstVertexIter v_iter = m.vertices_sbegin();
       v_iter != m.vertices_end();
       ++v_iter)
    {
    std::cout << "Vertex " << *v_iter << ": (" << m.point(*v_iter) << ")\n";
    }
}

// ----------------------------------------------------------------------

void
print_edges(MeshGeometry const& m)
{
  for (MeshGeometry::ConstEdgeIter e_iter = m.edges_sbegin();
       e_iter != m.edges_end();
       ++e_iter)
    {
    std::cout << "Edge " << *e_iter << ": "
              << "halfedges "
              << m.halfedge_handle(*e_iter, 0) << ", "
              << m.halfedge_handle(*e_iter, 1) << "; "
              << "is_boundary " << m.is_boundary(*e_iter)
              << "\n";
    }
}


// ----------------------------------------------------------------------

void
print_halfedges(MeshGeometry const& m)
{
  for (MeshGeometry::ConstHalfedgeIter h_iter = m.halfedges_begin();
       h_iter != m.halfedges_end();
       ++h_iter)
    {
    std::cout << "Halfedge " << *h_iter << " from "
              << m.from_vertex_handle(*h_iter)
              << " to "
              << m.to_vertex_handle(*h_iter)
              << ", face handle "
              << m.face_handle(*h_iter)
              << "\n";
    }
}

// ----------------------------------------------------------------------

void
print_faces(MeshGeometry const& m)
{
  for (MeshGeometry::ConstFaceIter f_iter = m.faces_sbegin();
       f_iter != m.faces_end();
       ++f_iter)
    {
    std::cout << "Face " << *f_iter << ": "
              << "vertex handles";

    for (MeshGeometry::ConstFaceVertexIter v_iter = m.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      std::cout << " " << *v_iter;
      }

    std::cout << ", vertex positions";

    for (MeshGeometry::ConstFaceVertexIter v_iter = m.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      std::cout << " (" << m.point(*v_iter) << ")";
      }
    std::cout << "\n";
    }
}

// ----------------------------------------------------------------------

void
print_mesh(MeshGeometry const& mesh)
{
  print_vertices(mesh);
  print_halfedges(mesh);
  print_edges(mesh);
  print_faces(mesh);
}

// ----------------------------------------------------------------------

void
test_1x1()
{
  MeshGeometry my_mesh;
  std::vector<VertexHandle> vertices;
  std::vector<VertexHandle> this_face;
  std::vector<EdgeHandle>   edges;

  vertices.push_back(my_mesh.add_vertex(MeshGeometry::Point(0, 0, 0)));
  vertices.push_back(my_mesh.add_vertex(MeshGeometry::Point(1, 0, 0)));
  vertices.push_back(my_mesh.add_vertex(MeshGeometry::Point(0, 1, 0)));
  vertices.push_back(my_mesh.add_vertex(MeshGeometry::Point(1, 1, 0)));

  this_face.push_back(vertices[0]);
  this_face.push_back(vertices[1]);
  this_face.push_back(vertices[2]);
  my_mesh.add_face(this_face);

  this_face.clear();
  this_face.push_back(vertices[1]);
  this_face.push_back(vertices[3]);
  this_face.push_back(vertices[2]);
  my_mesh.add_face(this_face);

  for (MeshGeometry::ConstEdgeIter e_iter = my_mesh.edges_sbegin();
       e_iter != my_mesh.edges_end();
       ++e_iter)
    {
    edges.push_back(*e_iter);
    }

  std::cout << "*** Mesh before split:\n";
  print_mesh(my_mesh);

  VertexHandle new_vertex(my_mesh.add_vertex(MeshGeometry::Point(0.5, 0.5, 0)));
  my_mesh.split_copy(edges[1], new_vertex);

  std::cout << "\n\n*** Mesh after split:\n";
  print_mesh(my_mesh);
}

// ----------------------------------------------------------------------


void
test_2x2()
{
  MeshGeometry my_mesh;
  std::vector<VertexHandle> vertices;
  std::vector<EdgeHandle>   edges;
  int rows=3, columns=3;
  
  for (int r = 0; r < 3; ++r)
    {
    for (int c = 0; c < 3; ++c)
      {
      vertices.push_back(my_mesh.add_vertex(MeshGeometry::Point(c, r, 0)));
      }
    }

  for (int r = 0; r < 2; ++r)
    {
    std::vector<VertexHandle> this_face;

    for (int c = 0; c < 2; ++c)
      {
      this_face.push_back(vertices[r*columns + c]);
      this_face.push_back(vertices[r*columns + (c+1)]);
      this_face.push_back(vertices[(r+1)*columns + c]);
      std::cout << "Next face: "
                << r*columns + c << " "
                << r*columns + (c+1) << " "
                << (r+1)*columns + c
                << "\n";
      my_mesh.add_face(this_face);
      this_face.clear();

      this_face.push_back(vertices[r*columns + (c+1)]);
      this_face.push_back(vertices[(r+1)*columns + (c+1)]);
      this_face.push_back(vertices[(r+1)*columns + c]);
      my_mesh.add_face(this_face);
      this_face.clear();

      std::cout << "Next face: "
                << r*columns + (c+1) << " "
                << (r+1)*columns + (c+1) << " "
                << (r+1)*columns + c << " "
                << "\n";

      }
    }

  for (MeshGeometry::ConstEdgeIter e_iter = my_mesh.edges_sbegin();
       e_iter != my_mesh.edges_end();
       ++e_iter)
    {
    edges.push_back(*e_iter);
    }

  std::cout << "*** Mesh before split:\n";
  print_mesh(my_mesh);

  VertexHandle new_vertex(my_mesh.add_vertex(MeshGeometry::Point(1.1, 1.1, 0)));
  my_mesh.split_copy(edges[8], new_vertex);

  std::cout << "\n\n*** Mesh after split:\n";
  print_mesh(my_mesh);

}

// ----------------------------------------------------------------------

int main(int argc, char* argv[])
{
  test_2x2();
  return 0;
}
