/*
 * The one and only purpose of this test is to see if we can compile
 * our Eigen + OpenMesh hybrid class.
 */

#include <iomanip>
#include <iostream>

#include <CrackTriangleMesh.h>

int main(int argc, char* argv[])
{
  CrackTriangleMesh mesh;

  mesh.debug_set_rectangular_triangle_mesh(101, 101);
  for (int i = 0; i < 40000; ++i)
    {
    if (i % 50 == 0)
      {
      std::ostringstream filebuf;
      filebuf << "separation_" << std::setw(5) << std::setfill('0') << i << ".pgm";
      mesh.debug_write_separation_image(101, 101, filebuf.str().c_str(), 1000);
      }

    if (i % 10000 == 0)
      {
      mesh.advance_simulation(0.01);
      }
    else
      {
      mesh.advance_simulation(0.01);
      }
//    mesh.add_uniform_contraction(0.025);
    }

  mesh.advance_simulation(0.001);

  mesh.debug_print_separation_eigenvalues();

  std::cout << "Success.\n";
  return 0;
}
