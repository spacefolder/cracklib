#include <iostream>
#include "CrackTriangleMesh.h"

// ----------------------------------------------------------------------

void
make_sample_mesh(MeshGeometry& mesh, int rows, int columns)
{
  std::vector<VertexHandle> vertices;

  for (int r = 0; r < rows; ++r)
    {
    for (int c = 0; c < columns; ++c)
      {
      vertices.push_back(mesh.add_vertex(MeshGeometry::Point(c, r, 0)));
      }
    }

  for (int r = 0; r < rows-1; ++r)
    {
    std::vector<VertexHandle> this_face;

    for (int c = 0; c < columns-1; ++c)
      {
      this_face.push_back(vertices[r*columns + c]);
      this_face.push_back(vertices[r*columns + (c+1)]);
      this_face.push_back(vertices[(r+1)*columns + c]);
      mesh.add_face(this_face);
      this_face.clear();

      this_face.push_back(vertices[r*columns + (c+1)]);
      this_face.push_back(vertices[(r+1)*columns + (c+1)]);
      this_face.push_back(vertices[(r+1)*columns + c]);
      mesh.add_face(this_face);
      this_face.clear();
      }
    }
}

// ----------------------------------------------------------------------


// ----------------------------------------------------------------------

void
print_vertices(MeshGeometry const& m)
{
  for (MeshGeometry::ConstVertexIter v_iter = m.vertices_sbegin();
       v_iter != m.vertices_end();
       ++v_iter)
    {
    std::cout << "Vertex " << *v_iter << ": (" << m.point(*v_iter) << ")\n";
    }
}

// ----------------------------------------------------------------------

void
print_edges(MeshGeometry const& m)
{
  for (MeshGeometry::ConstEdgeIter e_iter = m.edges_sbegin();
       e_iter != m.edges_end();
       ++e_iter)
    {
    std::cout << "Edge " << *e_iter << ": "
              << "halfedges "
              << m.halfedge_handle(*e_iter, 0) << ", "
              << m.halfedge_handle(*e_iter, 1) << "; "
              << "is_boundary " << m.is_boundary(*e_iter)
              << "\n";
    }
}


// ----------------------------------------------------------------------

void
print_halfedges(MeshGeometry const& m)
{
  for (MeshGeometry::ConstHalfedgeIter h_iter = m.halfedges_begin();
       h_iter != m.halfedges_end();
       ++h_iter)
    {
    std::cout << "Halfedge " << *h_iter << " from "
              << m.from_vertex_handle(*h_iter)
              << " to "
              << m.to_vertex_handle(*h_iter)
              << ", face handle "
              << m.face_handle(*h_iter)
              << "\n";
    }
}

// ----------------------------------------------------------------------

void
print_faces(MeshGeometry const& m)
{
  for (MeshGeometry::ConstFaceIter f_iter = m.faces_sbegin();
       f_iter != m.faces_end();
       ++f_iter)
    {
    std::cout << "Face " << *f_iter << ": "
              << "vertex handles";

    for (MeshGeometry::ConstFaceVertexIter v_iter = m.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      std::cout << " " << *v_iter;
      }

    std::cout << ", vertex positions";

    for (MeshGeometry::ConstFaceVertexIter v_iter = m.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      std::cout << " (" << m.point(*v_iter) << ")";
      }
    std::cout << "\n";
    }
}

// ----------------------------------------------------------------------

void
print_mesh(MeshGeometry const& mesh)
{
  print_vertices(mesh);
  print_halfedges(mesh);
  print_edges(mesh);
  print_faces(mesh);
}

// ----------------------------------------------------------------------

int main(int argc, char* argv[])
{
  MeshGeometry mesh;

  make_sample_mesh(mesh, 4, 4);

  std::cout << "Mesh before vertex split:\n";
  print_mesh(mesh);

  std::vector<VertexHandle> all_vertices(mesh.vertices_sbegin(), mesh.vertices_end());

  VertexHandle new_vertex(mesh.add_vertex(MeshGeometry::Point(1.25, 1.25, 0)));

  mesh.vertex_split(new_vertex,
                    all_vertices[5],
                    all_vertices[2],
                    all_vertices[8]);

  std::cout << "\n\nMesh after vertex split:\n";
  print_mesh(mesh);

  return 0;
}
