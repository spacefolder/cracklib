/*
 * The one and only purpose of this test is to see if we can compile
 * our Eigen + OpenMesh hybrid class.
 */

#include <iostream>

#include <CrackTriangleMesh.h>
#include "MeshGeneration.h"

int main(int argc, char* argv[])
{

  MeshGeometry initial_mesh;
  create_uniform_rectangle_mesh_diamonds(4, 4, 4, 4, initial_mesh);

  CrackTriangleMesh mesh(initial_mesh);
  mesh.fix_boundary_edges();
  mesh.set_material_toughness(1);
  mesh.add_uniform_contraction(0.9);
  mesh.set_verbosity(4);
  
  for (int i = 0; i < 1000; ++i)
    {
    mesh.advance_simulation(0.01);
    mesh.add_uniform_contraction(0.01);
    }
  mesh.advance_simulation(0.01);

  std::cout << "Success.\n";
  return 0;
}
