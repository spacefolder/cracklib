#ifndef __CrackedMesh_h
#define __CrackedMesh_h


#include "CrackTriangleMesh.h"


class CrackEngine
{
public:
  CrackEngine();
  CrackEngine(CrackTriangleMesh const& input_mesh);
  ~CrackEngine();

  virtual void set_startup_mesh(CrackTriangleMesh const& input_mesh);
  virtual void initialize_stress();

  void relax_stress_field();
  void update_failure_queue();
  virtual void update_stress_field();
  void crack_mesh_at_node(node_handle where);

  CrackTriangleMesh const& current_mesh() const;
  

private:
  CrackTriangleMesh CurrentMesh;
  
};

#endif
