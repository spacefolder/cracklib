/*
 * The one and only purpose of this test is to see if we can compile
 * our Eigen + OpenMesh hybrid class.
 */

#include <iostream>

#include <CrackTriangleMesh.h>

int main(int argc, char* argv[])
{
  CrackTriangleMesh mesh;
  mesh.set_material_toughness(100000000);

  mesh.debug_set_rectangular_triangle_mesh(9, 9);
  for (int i = 0; i < 1000; ++i)
    {
    mesh.advance_simulation(0.01);
    }
  mesh.advance_simulation(0.01);

  std::cout << "Success.\n";
  return 0;
}
