#include "MeshGeneration.h"
#include "CrackDataTypes.h"
#include "DelaunayTriangulation.h"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/polygon/polygon.hpp>
#include <boost/polygon/voronoi.hpp>

#include <boost/random.hpp>
#include <boost/random/uniform_int_distribution.hpp>



#include <iostream>
#include <vector>

typedef boost::polygon::point_data<int> MyPoint;
typedef boost::polygon::segment_data<int> MySegment;
typedef std::vector<MyPoint> point_vector_type;
typedef std::vector<MySegment> segment_vector_type;


namespace {

boost::random::mt19937 mersenne;

}

namespace distributions
{

typedef boost::random::poisson_distribution<int> poisson;
typedef boost::random::uniform_real_distribution<> uniform_real;
typedef boost::random::uniform_int_distribution<> uniform_int;

}

namespace generators
{

typedef boost::random::variate_generator<boost::random::mt19937, distributions::poisson > poisson;
typedef boost::random::variate_generator<boost::random::mt19937, distributions::uniform_real > uniform_real;
typedef boost::random::variate_generator<boost::random::mt19937, distributions::uniform_int > uniform_int;

}



// ----------------------------------------------------------------------

void lloyd_respace_points(point_vector_type& points)
{
  namespace bp = boost::polygon;
  namespace bg = boost::geometry;

  bp::voronoi_diagram<double> vd;
  segment_vector_type empty_segments;

  bp::construct_voronoi(
    points.begin(), points.end(),
    empty_segments.begin(), empty_segments.end(),
    &vd);

  typedef bg::model::d2::point_xy<double> point_xy;
  typedef bg::model::polygon<point_xy, true, true> polygon_xy;
  typedef bp::voronoi_edge<double> voronoi_edge_type;
  typedef bp::voronoi_vertex<double> voronoi_vertex_type;

  for (bp::voronoi_diagram<double>::const_cell_iterator iter = vd.cells().begin();
         iter != vd.cells().end();
         ++iter)
    {
    std::size_t voronoi_site_index = 0;
    if (iter->contains_point())
      {
      voronoi_site_index = iter->source_index();
      point_xy central_point(points[voronoi_site_index].x(), points[voronoi_site_index].y());

      std::vector<point_xy> vertices_in_voronoi_cell;
      polygon_xy current_cell_polygon;

      const voronoi_edge_type *starting_cell_edge = iter->incident_edge();
      const voronoi_edge_type *current_cell_edge = starting_cell_edge;
      bool ignore_cell = false;

      // Get a list of the vertices of the Voronoi cell containing
      // this point and construct a boost::geometry polygon from them.
      // If we happen to hit an infinite edge then just ignore this
      // polygon.

      do
        {
        if (current_cell_edge->is_infinite())
          {
          // Ignore the vertices on the boundary.  Just leave them
          // where they are.
          ignore_cell = true;
          break;
          }
        else
          {
          const voronoi_vertex_type* start_vertex = current_cell_edge->vertex0();
          boost::geometry::append(current_cell_polygon.outer(),
                                  point_xy(start_vertex->x(), start_vertex->y()));
          current_cell_edge = current_cell_edge->next();
          }
        }
      while (current_cell_edge != starting_cell_edge);

      if (ignore_cell) continue; // skip boundary cells

      // move the current point to the centroid of its Voronoi cell
      point_xy cell_centroid;
      boost::geometry::centroid(current_cell_polygon, cell_centroid);
      bp::x(points[voronoi_site_index], static_cast<int>(bg::get<0>(cell_centroid)));
      bp::y(points[voronoi_site_index], static_cast<int>(bg::get<1>(cell_centroid)));
      }
    }
}

// ----------------------------------------------------------------------

void create_vertex_grid(int columns, int rows, float r_width, float r_height, MeshGeometry& mesh, std::vector<VertexHandle>& vertices)
{
  vertices.clear();

  std::cout << "create_vertex_grid: Creating "
            << columns+1 << " x " << rows+1 << " grid of vertices to fit in rectangle with size " << r_width << " x " << r_height << "\n";
  
  float row_step = r_height / rows;
  float col_step = r_width / columns;

  for (int r = 0; r < (rows+1); ++r)
    {
    float row_coord = r * row_step;
    for (int c = 0; c < (columns+1); ++c)
      {
      float column_coord = c * col_step;

      vertices.push_back(mesh.add_vertex(MeshGeometry::Point(column_coord, row_coord, 0)));
      }
    }
}

// ----------------------------------------------------------------------

void create_rectangular_mesh(int rows, int columns, float r_width, float r_height, MeshGeometry& output)
{
  std::vector<VertexHandle> vertices;

  create_vertex_grid(rows, columns, r_width, r_height, output, vertices);

  // Assemble the vertices into faces
  for (int r = 0; r < rows; ++r)
    {
    for (int c = 0; c < columns; ++c)
      {
      std::vector<VertexHandle> this_face;

      this_face.push_back(vertices[ r*columns + c ]);

      this_face.push_back(vertices[ r*columns + (c+1) ]);
      this_face.push_back(vertices[ (r+1)*columns + c ]);
      output.add_face(this_face);
      this_face.clear();

      this_face.push_back(vertices[ r*columns + (c+1) ]);
      this_face.push_back(vertices[ (r+1)*columns + (c+1) ]);
      this_face.push_back(vertices[ (r+1)*columns + c ]);
      output.add_face(this_face);
      }
    }
}


// ----------------------------------------------------------------------

// This is exactly the same as create_regular_mesh except that we flip
// half of the diagonal edges to give a friendlier tessellation.

void create_uniform_rectangle_mesh_diamonds(int rows, int columns, float r_width, float r_height, MeshGeometry& output)
{
  std::vector<VertexHandle> vertices;

  create_vertex_grid(rows, columns, r_width, r_height, output, vertices);

  // Assemble the vertices into faces
  int v_columns = columns + 1;
  int v_rows = rows + 1;
  
  for (int r = 0; r < rows; ++r)
    {
    for (int c = 0; c < columns; ++c)
      {
      std::vector<VertexHandle> this_face;

      if ((r+c) % 2 == 0)
        {
        this_face.push_back(vertices[ r*v_columns + c ]);
        this_face.push_back(vertices[ r*v_columns + (c+1) ]);
        this_face.push_back(vertices[ (r+1)*v_columns + c ]);
        output.add_face(this_face);
        this_face.clear();

        this_face.push_back(vertices[ r*v_columns + (c+1) ]);
        this_face.push_back(vertices[ (r+1)*v_columns + (c+1) ]);
        this_face.push_back(vertices[ (r+1)*v_columns + c ]);
        output.add_face(this_face);
        }
      else
        {
        this_face.push_back(vertices[ r*v_columns + c ]);
        this_face.push_back(vertices[ r*v_columns + (c+1) ]);
        this_face.push_back(vertices[ (r+1)*v_columns + (c+1) ]);
        output.add_face(this_face);
        this_face.clear();

        this_face.push_back(vertices[ r*v_columns + c ]);
        this_face.push_back(vertices[ (r+1)*v_columns + (c+1) ]);
        this_face.push_back(vertices[ (r+1)*v_columns + c ]);
        output.add_face(this_face);
        }
      }
    }
}

// ----------------------------------------------------------------------

void stratified_generate_points(int x_buckets,
                                int y_buckets,
                                float aspect_ratio,
                                int desired_num_points,
                                point_vector_type& output)
{
  // Stratify the mesh into 64 buckets so that we can mitigate the
  // clumpiness of the randomness
  int num_buckets = x_buckets * y_buckets;
  int expected_points_per_bucket = desired_num_points / num_buckets;

  std::cout << "DEBUG: stratified_generate_points: "
            << desired_num_points << " points requested, "
            << num_buckets << " buckets, "
            << expected_points_per_bucket << " points per bucket\n";

  generators::poisson draw_points_per_bucket(::mersenne, distributions::poisson(expected_points_per_bucket));
  generators::uniform_int draw_row(::mersenne, distributions::uniform_int(0, y_buckets-1));
  generators::uniform_int draw_column(::mersenne, distributions::uniform_int(0, x_buckets-1));
  generators::uniform_real draw_from_unit_interval(::mersenne, distributions::uniform_real(0, 1));
  generators::uniform_int draw_bucket_index(::mersenne, distributions::uniform_int(0, num_buckets - 1));

  // First find out how many points we have in each bucket
  typedef std::vector<int> int_vector;
  int_vector bucket_point_counts;
  int total_points = 0;

  for (int i = 0; i < x_buckets; ++i)
    {
    for (int j = 0; j < y_buckets; ++j)
      {
      int point_count = draw_points_per_bucket();
      std::cout << "DEBUG: Bucket at ("
                << i << ", " << j << ") will have "
                << point_count << " points in it\n";
      bucket_point_counts.push_back(point_count);
      total_points += point_count;
      }
    }

  std::cout << "DEBUG: Generated " << total_points << " points so far\n";
  // Adjust by adding or removing points until we have exactly the
  // number of vertices we want
  if (total_points < desired_num_points)
    {
    for (int i = 0; i < desired_num_points - total_points; ++i)
      {
      bucket_point_counts[draw_bucket_index()] += 1;
      }
    }
  else if (total_points > desired_num_points)
    {
    // This is a little bit trickier.  We want to remove each point
    // with equal probability.  Fortunately, Boost knows how to do this...
    std::vector<double> probabilities(num_buckets, 0);
    for (int i = 0; i < num_buckets; ++i)
      {
      probabilities[i] = static_cast<float>(bucket_point_counts[i]) / total_points;
      }
    boost::random::discrete_distribution<> bucket_dist(probabilities);

    for (int i = 0; i < desired_num_points - total_points; ++i)
      {
      int bucket_id = bucket_dist(::mersenne);
      while (bucket_point_counts[bucket_id] == 0)
        {
        bucket_id = bucket_dist(::mersenne);
        }
      std::cout << "Removing one point from bucket " << bucket_id << "\n";
      bucket_point_counts[bucket_id] -= 1;
      }
    }

  // Random note: 'bucket' is one of those odd words in the English
  // language that starts looking wrong after you type it enough
  // times.

  // at this point, the 'bucket_point_counts' array contains the
  // number of points we want to draw within each bucket.  Since the
  // Voronoi library works with integers we're going to generate these
  // on a huge square with integer coordinates.  2^24 possibilities on
  // a side should be more than enough resolution and we can still
  // represent it losslessly when we go back to floating-point land.
  int i_bucket_width, i_bucket_height;
  if (aspect_ratio > 1)
    {
    i_bucket_width = (1<<24) / x_buckets;
    i_bucket_height = static_cast<int>(i_bucket_width / aspect_ratio);
    }
  else
    {
    i_bucket_height = (1<<24) / y_buckets;
    i_bucket_width = static_cast<int>(i_bucket_height * aspect_ratio);
    }

  generators::uniform_int draw_x_coord(::mersenne, distributions::uniform_int(0, i_bucket_width - 1));
  generators::uniform_int draw_y_coord(::mersenne, distributions::uniform_int(0, i_bucket_height - 1));

  point_vector_type all_points;
  for (int row = 0; row < y_buckets; ++row)
    {
    int y_base_coord = row * i_bucket_height;
    for (int col = 0; col < x_buckets; ++col)
      {
      int x_base_coord = col * i_bucket_width;

      for (int i = 0; i < bucket_point_counts[row*x_buckets + col]; ++i)
        {
        output.push_back(::MyPoint(
                           x_base_coord + draw_x_coord(),
                           y_base_coord + draw_y_coord()
                           ));
        }
      }
    }
}


// ----------------------------------------------------------------------

void test_delaunay_mesh(int width, int height, MeshGeometry& output)
{
  std::vector<VertexHandle> vertices;
  generators::uniform_real draw_from_unit_interval(::mersenne, distributions::uniform_real(0, 1));

  create_vertex_grid(width, height, width, height, output, vertices);
  for (MeshGeometry::ConstVertexIter v_iter = output.vertices_begin();
       v_iter != output.vertices_end();
       ++v_iter)
    {
    MeshGeometry::Point& here(output.point(*v_iter));

    here[0] += 0.8 * (draw_from_unit_interval() - 0.5);
    here[1] += 0.8 * (draw_from_unit_interval() - 0.5);
    }

  // Now we go back into floating-point hand.
  std::vector<Vector3f> float_points;
  for (MeshGeometry::VertexIter v_iter = output.vertices_begin();
       v_iter != output.vertices_end();
       ++v_iter)
    {
    float_points.push_back(Vector3f(
                             output.point(*v_iter)[0],
                             output.point(*v_iter)[1],
                             0
                             ));
    }

  // Now we need a triangulation of those points to form the mesh.
  triangulate_points(float_points, output);
}

