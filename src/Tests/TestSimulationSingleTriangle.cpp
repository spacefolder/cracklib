/*
 * The one and only purpose of this test is to see if we can compile
 * our Eigen + OpenMesh hybrid class.
 */

#include <iostream>

#include <CrackTriangleMesh.h>

int main(int argc, char* argv[])
{
  CrackTriangleMesh mesh;

  mesh.debug_set_single_triangle_mesh();
  for (int i = 0; i < 10; ++i)
    {
    mesh.advance_simulation(0.001);
    }
  
  std::cout << "Success.\n";
  return 0;
}
