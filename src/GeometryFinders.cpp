#include "GeometryFinders.h"


#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>


namespace {


// ----------------------------------------------------------------------


std::pair<VertexHandle, VertexHandle>
find_symmetric_difference(
  std::vector<VertexHandle> const& face1_vertices,
  std::vector<VertexHandle> const& face2_vertices
  )
{
  std::pair<VertexHandle, VertexHandle> result;

  std::vector<VertexHandle> face1_unique, face2_unique;

  std::set_difference(
    face1_vertices.begin(), face1_vertices.end(),
    face2_vertices.begin(), face2_vertices.end(),
    std::back_inserter(face1_unique)
    );

  std::set_difference(
    face2_vertices.begin(), face2_vertices.end(),
    face1_vertices.begin(), face1_vertices.end(),
    std::back_inserter(face2_unique)
    );

  assert(face1_unique.size() == 1);
  assert(face2_unique.size() == 1);

  result.first = face1_unique[0];
  result.second = face2_unique[0];

  return result;
}

}

// ----------------------------------------------------------------------

std::pair<MeshGeometry::Point, MeshGeometry::Point>
compute_mesh_bounding_box(MeshGeometry const& mesh)
{
  bool first_vertex = true;
  MeshGeometry::Point min_corner, max_corner;

  for (MeshGeometry::ConstVertexIter v_iter = mesh.vertices_sbegin();
       v_iter != mesh.vertices_end();
       ++v_iter)
    {
    MeshGeometry::Point const& here(mesh.point(*v_iter));
    if (first_vertex)
      {
      min_corner = max_corner = here;
      first_vertex = false;
      }
    else
      {
      for (int i = 0; i < 3; ++i)
        {
        min_corner[i] = std::min(min_corner[i], here[i]);
        max_corner[i] = std::max(max_corner[i], here[i]);
        }
      }
    }

  return std::make_pair(min_corner, max_corner);
}

// ----------------------------------------------------------------------

HalfedgeHandle
find_opposite_halfedge(MeshGeometry const& mesh,
                       FaceHandle const& face,
                       VertexHandle const& center_vertex)
{
  for (MeshGeometry::ConstFaceHalfedgeIter halfedge_iter = mesh.cfh_begin(face);
       halfedge_iter.is_valid();
       ++halfedge_iter)
    {
    // We want the edge opposite the center vertex
    VertexHandle to_vertex =
      mesh.to_vertex_handle(*halfedge_iter);
    VertexHandle from_vertex =
      mesh.from_vertex_handle(*halfedge_iter);

    if (to_vertex != center_vertex && from_vertex != center_vertex)
      {
      return *halfedge_iter;
      }
    }

  assert(1==0 && "Couldn't find any halfedge opposite this vertex!");
  return HalfedgeHandle();
}

// ----------------------------------------------------------------------

FaceHandle
face_across_opposite_edge(MeshGeometry const& mesh,
                          FaceHandle const& face,
                          VertexHandle const& vertex)
{
  HalfedgeHandle opposite_halfedge(find_opposite_halfedge(mesh, face, vertex));
  assert(opposite_halfedge.is_valid());
  return mesh.opposite_face_handle(opposite_halfedge);
}

// ----------------------------------------------------------------------

void
get_face_vertex_handles(MeshGeometry const& mesh,
                        FaceHandle const& face,
                        std::vector<VertexHandle>& output)
{
  output.reserve(3);

  for (MeshGeometry::ConstFaceVertexIter v_iter = mesh.cfv_iter(face);
       v_iter.is_valid();
       ++v_iter)
    {
    output.push_back(*v_iter);
    }

  std::sort(output.begin(), output.end());
}

// ----------------------------------------------------------------------

VertexHandle
vertex_opposite_edge(MeshGeometry const& mesh, FaceHandle const& face, EdgeHandle const& edge)
{
  VertexHandle v1, v2;

  assert(face.is_valid());
  assert(edge.is_valid());

  // Identify the two vertices in the supplied edge
  HalfedgeHandle halfedge(mesh.halfedge_handle(edge, 0));
  v1 = mesh.from_vertex_handle(halfedge);
  v2 = mesh.to_vertex_handle(halfedge);

  // Circulate around the (half)-edges in the provided face.  Since
  // the face is a triangle, the vertex that is not v1 or v2 is the
  // one we want.
  for (MeshGeometry::ConstFaceHalfedgeIter he_iter = mesh.cfh_iter(face);
       he_iter.is_valid();
       ++he_iter)
    {
    VertexHandle v(mesh.from_vertex_handle(*he_iter));
    if (v != v1 && v != v2)
      {
      return v;
      }
    }

  assert(1==0 && "Couldn't find a vertex opposite the given edge!");
  return VertexHandle();
}

// ----------------------------------------------------------------------

FaceHandle find_face_containing(MeshGeometry const& mesh,
                                std::vector<VertexHandle> const& vertices)
{
  if (vertices.size() == 0)
    {
    return FaceHandle();
    }
  else
    {
    // Pick the first vertex out of the required list.  Loop around
    // all the faces attached to that vertex.  Check each one to see
    // if it contains all of the required vertices.  Return the first
    // one we find.

    VertexHandle first_vertex = vertices[0];
    for (MeshGeometry::ConstVertexFaceIter face_iter = mesh.cvf_iter(first_vertex);
         face_iter.is_valid();
         ++face_iter)
      {
      std::set<VertexHandle> vertices_remaining(vertices.begin(), vertices.end());
      for (MeshGeometry::ConstFaceVertexIter vertex_iter = mesh.cfv_iter(*face_iter);
           vertex_iter.is_valid();
           ++vertex_iter)
        {
        if (vertices_remaining.empty())
          {
          // found one!
          return *face_iter;
          }
        else
          {
          if (vertices_remaining.find(*vertex_iter) != vertices_remaining.end())
            {
            vertices_remaining.erase(*vertex_iter);
            }
          }
        }
      // We've made it all the way around the face.  Did we find everything?
      if (vertices_remaining.empty())
        {
        return *face_iter;
        }
      } // end of looping around faces

    // No luck.
    return FaceHandle();
    }
}

// ----------------------------------------------------------------------

EdgeHandle
find_opposite_edge(MeshGeometry const& mesh,
                   FaceHandle const& face,
                   VertexHandle const& vertex)
{
  HalfedgeHandle he_handle = find_opposite_halfedge(mesh, face, vertex);
  if (he_handle.is_valid())
    {
    return mesh.edge_handle(he_handle);
    }
  else
    {
    return EdgeHandle();
    }
}

// ----------------------------------------------------------------------

int
count_boundary_edges(FaceHandle const& face, MeshGeometry const& mesh)
{
  int count = 0;

  for (MeshGeometry::ConstFaceHalfedgeIter he_iter = mesh.cfh_iter(face);
       he_iter.is_valid();
       ++he_iter)
    {
    HalfedgeHandle opposite = mesh.opposite_halfedge_handle(*he_iter);
    if (mesh.is_boundary(opposite))
      {
      ++count;
      }
    }
  return count;
}

// ----------------------------------------------------------------------

HalfedgePair
find_perforation_corner_in_face(FaceHandle const& face, MeshGeometry const& mesh)
{
  for (MeshGeometry::ConstFaceHalfedgeIter he_iter = mesh.cfh_iter(face);
       he_iter.is_valid();
       ++he_iter)
    {
    HalfedgeHandle this_edge(*he_iter);
    HalfedgeHandle next_edge(mesh.next_halfedge_handle(this_edge));
    HalfedgeHandle o_this_edge(mesh.opposite_halfedge_handle(this_edge));
    HalfedgeHandle o_next_edge(mesh.opposite_halfedge_handle(next_edge));

    if (mesh.is_boundary(o_this_edge) && mesh.is_boundary(o_next_edge))
      {
      // found it!
      return HalfedgePair(this_edge, next_edge);
      }
    }
  std::cout << "WARNING: Couldn't find perforation corner in face "
            << face << "!\n";
  return HalfedgePair(HalfedgeHandle(), HalfedgeHandle());
}
