/*
 * This file contains functions to generate a variety of patterns for
 * stress images.  There is neither rhyme nor reason to what I've
 * chosen to implement.  You should feel free to add whatever you
 * want.  Have fun!
 */

#ifndef __cracklib_ExampleStressImages_h
#define __cracklib_ExampleStressImages_h

#include "CrackDataTypes.h"

/** Reverse (x = 1 - x) all pixel values in an image.
 */

void make_black_image(DynamicArray& image, int height, int width);
void make_white_image(DynamicArray& image, int height, int width);

void invert(DynamicArray& input_image);

/** Fill an image with a rectangle with specified color
 *
 * Given an input image, draw a rectangle in the middle.  The
 * rectangle will have the same aspect ratio as the image as a whole.
 * A fill fraction of 0 will give you just a point in the center.  A
 * fill fraction of 1 will fill the entire image.
 *
 * If you choose to provide a pixel value as the third argument, that
 * value will be used for the pixels that are set.  It defaults to 1.
 */

void inner_square(DynamicArray& input_image, float fill_fraction, float pixel_value=1);

int stress_image_width(DynamicArray const& image);
int stress_image_height(DynamicArray const& image);

#endif
