#include "CrackSimulationMath.h"
#include <iostream>


int main(int argc, char* argv[])
{
  Matrix2f tensor;
  tensor << 2, 0, 0, 2;
  Matrix2f compressive, tensile;

  decompose_stress_tensor(tensor, compressive, tensile);

  std::cout << "Compressive part of tensor:\n" << compressive << "\n";
  std::cout << "Tensile part of tensor:\n" << tensile << "\n";

  return 0;
}
