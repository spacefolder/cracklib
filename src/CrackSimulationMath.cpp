#include "CrackSimulationMath.h"

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/LU>
#include <iostream>
#include <cmath>

#include <boost/math/constants/constants.hpp>

template<typename real>
bool almost_equal(real a, real b, real tolerance=1e-4)
{
  return (fabs(a-b) < tolerance);
}

// ----------------------------------------------------------------------

double angle_between(Vector3f const& v1, Vector3f const& v2)
{
  double divisor = v1.norm() * v2.norm();
  assert(divisor != 0);

  double cos_angle = v1.dot(v2) / divisor;
  if (cos_angle < -1) cos_angle = -1;
  if (cos_angle > 1) cos_angle = 1;

  return 180.0 * acos(cos_angle) / boost::math::constants::pi<double>();
}

// ----------------------------------------------------------------------

template<typename matrix_type>
bool check_for_symmetry(matrix_type const& matrix)
{
  for (int row = 0; row < matrix.rows(); ++row)
    {
    for (int col = row+1; col < matrix.cols(); ++col)
      {
      if (!almost_equal(matrix(row, col), matrix(col, row)))
        {
//        std::cout << "Matrix is asymmetric\n";
        return false;
        }
      }
    }
//  std::cout << "Matrix is symmetric\n";
  return true;
}

// ----------------------------------------------------------------------

EigenPair3 largest_eigenpair(Matrix3f const& matrix)
{
//  std::cout << "matrix in largest_eigenpair: ";
  check_for_symmetry(matrix);

  typedef Eigen::SelfAdjointEigenSolver<Matrix3f> SolverType;
  SolverType solver(matrix);

  Matrix3f evecs(solver.eigenvectors().real());
  Vector3f evals(solver.eigenvalues().real());

  float e_value(-1);
  float largest_e_value, smallest_e_value;

  Vector3f corresponding_evec;
  for (std::size_t i = 0; i < evals.size(); ++i)
    {
    float e_value = evals(i);
    Vector3f e_vector = evecs.col(i);

    if (i == 0)
      {
      largest_e_value = e_value;
      smallest_e_value = e_value;
      corresponding_evec = evecs.col(i);
      }
    else
      {
      if (e_value > largest_e_value)
        {
        largest_e_value = e_value;
        corresponding_evec = e_vector;
        }
      if (e_value < smallest_e_value)
        {
        smallest_e_value = e_value;
        }
      }
    }

  return EigenPair3(largest_e_value, corresponding_evec);
}

// ----------------------------------------------------------------------

void make_orthonormal_basis(std::vector<Vector3f> const& input_points,
                            std::vector<Vector3f>&  basis)
{
  basis.reserve(2);

  Vector3f v1 = input_points[1] - input_points[0];
  Vector3f v2 = input_points[2] - input_points[0];

  // Make v2 perpendicular to v1 using Gram-Schmidt orthonormalization
  v2 = v2 - (v1.dot(v2) / v1.dot(v1)) * v1;

  basis[0] = v1.normalized();
  basis[1] = v2.normalized();
}


// ----------------------------------------------------------------------

Vector2f make_perpendicular_vector(Vector2f const& input)
{
  return Vector2f(-input(1), input(0));
}


// ----------------------------------------------------------------------

void project_triangle_vertices_to_local_space(std::vector<Vector3f> const& input_vertices,
                                              std::vector<Vector2f>& output_vertices)

{
  output_vertices.reserve(3);
  std::vector<Vector3f> ortho_basis;
  ortho_basis.reserve(2);

  make_orthonormal_basis(input_vertices, ortho_basis);
  output_vertices.push_back(Vector2f::Zero());
  output_vertices.push_back(project_vector_into_triangle_space(input_vertices[1] - input_vertices[0], ortho_basis));
  output_vertices.push_back(project_vector_into_triangle_space(input_vertices[2] - input_vertices[0], ortho_basis));
}

// ----------------------------------------------------------------------

Vector2f project_vector_into_triangle_space(Vector3f const& vector,
                                            std::vector<Vector3f> const& basis)
{
  return Vector2f(
    vector.dot(basis[0]),
    vector.dot(basis[1])
    );
}

// ----------------------------------------------------------------------

Matrix3f construct_barycentric_basis(std::vector<Vector3f> const& vertices)
{
  Matrix3f result(Matrix3f::Ones());
  std::vector<Vector2f> in_plane_coordinates;
//  assert(1==0);

  project_triangle_vertices_to_local_space(vertices, in_plane_coordinates);

  // first column coordinates are 0 by construction
  result(0, 0) = 0;
  result(1, 0) = 0;

  // second and third column are in-plane coordinates of vertices 1 and 2
  result(0, 1) = in_plane_coordinates[1](0);
  result(1, 1) = in_plane_coordinates[1](1);

  result(0, 2) = in_plane_coordinates[2](0);
  result(1, 2) = in_plane_coordinates[2](1);

  return result.inverse();
}

// ----------------------------------------------------------------------

Matrix2f construct_matrix_with_desired_eigenvector(Vector2f const& evec)
{
  ensure_no_nans(evec);

  if (evec.dot(evec) == 0)
    {
    return Matrix2f::Zero();
    }
  else
    {
    return (1.0 / evec.norm()) * (evec * evec.transpose());
    }
}


// ----------------------------------------------------------------------

Matrix3f construct_matrix_with_desired_eigenvector(Vector3f const& evec)
{
  ensure_no_nans(evec);

  if (evec.dot(evec) == 0)
    {
    return Matrix3f::Zero();
    }
  else
    {
    return (1.0 / evec.norm()) * (evec * evec.transpose());
    }
}

// ----------------------------------------------------------------------

void decompose_stress_tensor(Matrix2f const& stress_tensor,
                             Matrix2f& compressive,
                             Matrix2f& tensile)
{
  compressive = Matrix2f::Zero();
  tensile = Matrix2f::Zero();

  typedef Eigen::SelfAdjointEigenSolver<Matrix2f> SolverType;
  SolverType solver(stress_tensor);

//  std::cout << "stress tensor: ";
  check_for_symmetry(stress_tensor);

  Matrix2f evecs(solver.eigenvectors().real());
  Vector2f evals(solver.eigenvalues().real());

  for (std::size_t i = 0; i < evals.size(); ++i)
    {
    float e_value = evals(i);
    Vector2f e_vector = evecs.col(i);

    if (i == 1 && evals(0) == evals(1))
      {
      e_vector = make_perpendicular_vector(evecs.col(0));
      }

    if (e_value != 0)
      {
      Matrix2f partial_tensor = e_value * construct_matrix_with_desired_eigenvector(e_vector);
      if (e_value > 0)
        {
        tensile += partial_tensor;
        }
      else
        {
        compressive += partial_tensor;
        }
      }
    }
#if 0
  std::cout << "Input tensor:\n" << stress_tensor << "\n";
  std::cout << "Eigenvalues: " << evals << "\n";
  std::cout << "Eigenvectors: " << evecs << "\n";
#endif
}

// ----------------------------------------------------------------------


double intersect_plane_with_line(Vector3f const& plane_point,
                                 Vector3f const& plane_normal,
                                 Vector3f const& line_begin,
                                 Vector3f const& line_direction,
                                 bool *solution_exists)
{
  if (fabs(plane_normal.dot(line_direction)) < 0.0001)
    {
    *solution_exists = false;
    return 0;
    }

  // The plane is the set of points p for which (p - plane_point) dot
  // plane_normal = 0.
  //
  // Let's define our line as the points L such that L = line_begin +
  // d * line_direction where d is real.
  //
  // Substituting, we have
  //
  // (d * line_direction + line_begin - plane_point) dot plane_normal = 0
  //
  // Solve for d:
  //
  // d = ((plane_point - line_begin) dot plane_normal) / (line_direction dot plane_normal)

  if (solution_exists != 0)
    {
    *solution_exists = true;
    }
  double d = (plane_point - line_begin).dot(plane_normal) / (plane_normal.dot(line_direction));
  return d;
}

// ----------------------------------------------------------------------

double triangle_aspect_ratio(Vector3f const& A,
                             Vector3f const& B,
                             Vector3f const& C)
{
  double a = (A - B).norm();
  double b = (A - C).norm();
  double c = (B - C).norm();
  double s = (a + b + c) * 0.5;

  return (a * b * c) / (8 * (s-a) * (s-b) * (s-c));
}
