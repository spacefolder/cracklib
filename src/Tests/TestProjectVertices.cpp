#include "CrackSimulationMath.h"
#include <iostream>

int main(int argc, char* argv[])
{
  std::vector<Vector3f> corners(3);
  corners[0] = Vector3f(1, 0, 0);
  corners[1] = Vector3f(0, 5, 0);
  corners[2] = Vector3f(10, 16, 0);

  std::vector<Vector2f> projected;

  project_triangle_vertices_to_local_space(corners, projected);

  std::cout << "Input vectors: "
            << "(" << corners[0].transpose() << "), "
            << "(" << corners[1].transpose() << "), "
            << "(" << corners[2].transpose() << ")\n";

  std::cout << "Corners in plane of triangle: "
            << "(" << projected[0].transpose() << "), "
            << "(" << projected[1].transpose() << "), "
            << "(" << projected[2].transpose() << ")\n";

  // try a more intuitively interpretable example
  corners[0] = Vector3f(10, 10, 10);
  corners[1] = Vector3f(0, 10, 0);
  corners[2] = Vector3f(10, 0, 0);


  project_triangle_vertices_to_local_space(corners, projected);

  std::cout << "\n\n";
  std::cout << "Input vectors: "
            << "(" << corners[0].transpose() << "), "
            << "(" << corners[1].transpose() << "), "
            << "(" << corners[2].transpose() << ")\n";

  std::cout << "Corners in plane of triangle: "
            << "(" << projected[0].transpose() << "), "
            << "(" << projected[1].transpose() << "), "
            << "(" << projected[2].transpose() << ")\n";

  return 0;
}
