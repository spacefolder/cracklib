#include "DelaunayTriangulation.h"
#include "CrackSimulationMath.h"
#include "external/delaunay/delaunay.h"
#include <Eigen/Dense>
#include <cmath>

namespace {

Vec2f vector3_to_vec2(Vector3f const& in)
{
  return Vec2f(in[0], in[1]);
}

Vector3f vec2_to_vector3(Vec2f const& in)
{
  return Vector3f(in.x, in.y, 0);
}

MeshGeometry::Point vector3_to_point(Vector3f const& in)
{
  return MeshGeometry::Point(in[0], in[1], in[2]);
}

}

void triangulate_points(std::vector<Vector3f> const& input,
                        MeshGeometry& output)
{
  std::vector<Vec2f> converted_points;
  std::size_t point_index = 0;

  for (std::vector<Vector3f>::const_iterator iter = input.begin();
       iter != input.end();
       ++iter, ++point_index)
    {
    Vec2f cvt = ::vector3_to_vec2(*iter);
    cvt.index = point_index;
    converted_points.push_back(cvt);
    }

  Delaunay triangulator;
  triangulator.triangulate(converted_points);

  std::vector<VertexHandle> handles;
  for (std::vector<Vector3f>::const_iterator iter = input.begin();
       iter != input.end();
       ++iter)
    {
    handles.push_back(output.add_vertex(::vector3_to_point(*iter)));
    }

  std::vector<VertexHandle> this_face(3);
  int num_faces_so_far = 0;
  for (std::vector<Triangle>::const_iterator iter = triangulator.getTriangles().begin();
       iter != triangulator.getTriangles().end();
       ++iter, ++num_faces_so_far)
    {
    Vector3f v1(input[iter->p1.index]);
    Vector3f v2(input[iter->p2.index]);
    Vector3f v3(input[iter->p3.index]);

    double aspect = triangle_aspect_ratio(v1, v2, v3);
    if (aspect > 10)
      {
#if defined(COPIOUS_DEBUG_OUTPUT)
      std::cout << "DEBUG: Skipping face ("
                << iter->p1.index << ", "
                << iter->p2.index << ", "
                << iter->p3.index << ") because of unpleasant aspect ratio "
                << aspect << "\n";
      #endif
      continue;
      }

    this_face[0] = handles[iter->p1.index];
    this_face[1] = handles[iter->p2.index];
    this_face[2] = handles[iter->p3.index];

    if (output.is_boundary(this_face[0]) == false)
      {
      std::cout << "ERROR: Vertex " << iter->p1.index << " is not on the boundary!\n";
      }
    if (output.is_boundary(this_face[1]) == false)
      {
      std::cout << "ERROR: Vertex " << iter->p2.index << " is not on the boundary!\n";
      }
    if (output.is_boundary(this_face[2]) == false)
      {
      std::cout << "ERROR: Vertex " << iter->p3.index << " is not on the boundary!\n";
      }

    Vector3f leg1 = input[iter->p2.index] - input[iter->p1.index];
    Vector3f leg2 = input[iter->p3.index] - input[iter->p1.index];

    Vector3f cross = leg1.cross(leg2);

    if (cross(2) > 0)
      {
      // make sure to add vertices in CCW order
      VertexHandle tmp = this_face[0];
      this_face[0] = this_face[2];
      this_face[2] = tmp;
      }

    FaceHandle face = output.add_face(this_face);
    if (!face.is_valid())
      {
      std::cout << "ERROR: Found invalid face " << num_faces_so_far << "!\n";
      std::cout << "Vertices:\n";
      std::cout << iter->p1.index << " ("
                << input[iter->p1.index].transpose() << ")\n";
      std::cout << iter->p2.index << " ("
                << input[iter->p2.index].transpose() << ")\n";
      std::cout << iter->p3.index << " ("
                << input[iter->p3.index].transpose() << ")\n";
      }
    }
}
