#ifndef __OpenMeshEigenTraits_h
#define __OpenMeshEigenTraits_h

#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>

struct OpenMeshEigenTraits : public OpenMesh::DefaultTraits
{
  typedef Eigen::Vector3f Point;
  typedef Eigen::Vector3f Normal;
};

typedef OpenMesh::TriMesh_ArrayKernelT<OpenMeshEigenTraits> EigenTriangleMesh;

#endif
