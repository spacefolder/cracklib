#include "ColoredCrackMesh.h"
#include <cmath>
#include <iostream>

ColoredCrackMesh::ColoredCrackMesh()
{
  this->request_mesh_renderable_properties();
  this->GarbageCollectionInterval = 50;
  this->NumCrackEvents = 0;
}

// ----------------------------------------------------------------------

ColoredCrackMesh::ColoredCrackMesh(MeshGeometry const& initial_mesh)
  : CrackTriangleMesh(initial_mesh)
{
  this->GarbageCollectionInterval = 50;
  this->NumCrackEvents = 0;
  this->request_mesh_renderable_properties();
}

// ----------------------------------------------------------------------

ColoredCrackMesh::~ColoredCrackMesh()
{
  // nothing to do here
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::advance_simulation(float how_long)
{
  this->NumCrackEvents = 0;

  if (this->CurrentIteration > 0 &&
      this->CurrentIteration % this->GarbageCollectionInterval == 0)
    {
    std::cout << "Invoking garbage collection at iteration "
              << this->CurrentIteration << "\n";
    this->garbage_collect_mesh();
    }

  this->CrackTriangleMesh::advance_simulation(how_long);
  this->update_vertex_colors();
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::collect_crack_anchor_vertices()
{
  this->CrackAnchorVertices.clear();
  for (MeshGeometry::ConstVertexIter v_iter = this->MaterialMesh.vertices_sbegin();
       v_iter != this->MaterialMesh.vertices_end();
       ++v_iter)
    {
    if (this->MaterialMesh.property(IsCrackAnchor, *v_iter))
      {
      this->CrackAnchorVertices.push_back(*v_iter);
      }
    }
}

// ----------------------------------------------------------------------

VertexHandle
ColoredCrackMesh::duplicate_vertex(VertexHandle const& v)
{
  VertexHandle duplicate(this->CrackTriangleMesh::duplicate_vertex(v));
  this->MaterialMesh.property(this->IsCrackAnchor, duplicate) = true;
  this->CrackAnchorVertices.push_back(duplicate);
  this->NumCrackEvents += 1;
  return duplicate;
}

// ----------------------------------------------------------------------

int
ColoredCrackMesh::extract_boundary_loop(VertexHandle const& starting_vertex,
                                        std::vector<unsigned int>& indices)
{
  HalfedgeHandle current_halfedge;

  // Find a boundary halfedge adjacent to this vertex.
  for (MeshGeometry::ConstVertexOHalfedgeIter h_iter = this->MaterialMesh.cvoh_iter(starting_vertex);
       h_iter.is_valid();
       ++h_iter)
    {
    if (this->MaterialMesh.property(this->AlreadyTraversed, *h_iter))
      {
      continue;
      }
    if (this->MaterialMesh.is_boundary(*h_iter))
      {
      current_halfedge = *h_iter;
      break;
      }
    }

  if (current_halfedge.is_valid() == false)
    {
    return 0;
    }

  while (this->MaterialMesh.property(this->AlreadyTraversed, current_halfedge) == false)
    {
    assert(this->MaterialMesh.is_boundary(current_halfedge));
//    this->debug_print_halfedge(current_halfedge);

    this->MaterialMesh.property(this->AlreadyTraversed, current_halfedge) = true;
    indices.push_back(this->MaterialMesh.from_vertex_handle(current_halfedge).idx());
    indices.push_back(this->MaterialMesh.to_vertex_handle(current_halfedge).idx());
    current_halfedge = this->MaterialMesh.next_halfedge_handle(current_halfedge);
    }

  assert(indices.size() % 2 == 0);

  return 1;
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::extract_boundary_loops(std::vector<unsigned int>& indices)
{
  this->initialize_already_traversed();
  indices.clear();

  int num_loops = 0;
  for (std::vector<VertexHandle>::const_iterator v_iter = this->CrackAnchorVertices.begin();
       v_iter != this->CrackAnchorVertices.end();
       ++v_iter)
    {
    num_loops += this->extract_boundary_loop(*v_iter, indices);
    }
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::garbage_collect_mesh()
{
  return;
  this->MaterialMesh.garbage_collection();
  std::cout << "After garbage collection, material mesh has "
            << this->MaterialMesh.n_vertices() << " vertices, "
            << this->MaterialMesh.n_edges() << " edges, and "
            << this->MaterialMesh.n_faces() << " faces.\n";
  
  this->collect_crack_anchor_vertices();
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::initialize_already_traversed()
{
  for (MeshGeometry::ConstHalfedgeIter h_iter = this->MaterialMesh.halfedges_begin();
       h_iter != this->MaterialMesh.halfedges_end();
       ++h_iter)
    {
    this->MaterialMesh.property(this->AlreadyTraversed, *h_iter) = false;
    }
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::initialize_is_crack_anchor()
{
  for (MeshGeometry::ConstVertexIter v_iter = this->MaterialMesh.vertices_sbegin();
       v_iter != this->MaterialMesh.vertices_end();
       ++v_iter)
    {
    this->MaterialMesh.property(this->IsCrackAnchor, *v_iter) = false;
    }
}

// ----------------------------------------------------------------------

Vector3f
ColoredCrackMesh::material_vertex_color(VertexHandle const& v) const
{
  MeshGeometry::Color const& uc_color(this->MaterialMesh.color(v));
  return Vector3f(
    uc_color[0] / 255.0,
    uc_color[1] / 255.0,
    uc_color[2] / 255.0
    );
}

// ----------------------------------------------------------------------

int
ColoredCrackMesh::num_crack_events() const
{
  return this->NumCrackEvents;
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::request_mesh_renderable_properties()
{
  this->MaterialMesh.request_vertex_colors();
  this->CrackMesh.request_vertex_colors();
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::set_initial_geometry(MeshGeometry const& g)
{
  this->CrackTriangleMesh::set_initial_geometry(g);
  this->MaterialMesh.add_property(this->AlreadyTraversed);
  this->MaterialMesh.add_property(this->IsCrackAnchor);
  this->initialize_already_traversed();
  this->initialize_is_crack_anchor();
  this->request_mesh_renderable_properties();
  this->update_vertex_colors();
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::update_vertex_colors()
{
  for (MeshGeometry::ConstVertexIter v_iter = this->MaterialMesh.vertices_sbegin();
       v_iter != this->MaterialMesh.vertices_end();
       ++v_iter)
    {
    this->compute_vertex_color(*v_iter);
    }
}

// ----------------------------------------------------------------------

void
ColoredCrackMesh::compute_vertex_color(VertexHandle const& vertex)
{
  float separation_eigenvalue = this->MaterialMesh.property(this->LargestSeparationEigenvalue, vertex);
  float toughness = this->MaterialMesh.property(this->MaterialToughness, vertex);

  float how_close_to_failure = separation_eigenvalue / toughness;
  if (how_close_to_failure < 0) how_close_to_failure = 0;
  if (how_close_to_failure > 1) how_close_to_failure = 1;

  unsigned char red = 255;
  unsigned char green = 255 - static_cast<int>(floor(255 * how_close_to_failure));
  unsigned char blue = 255 - static_cast<int>(floor(255 * how_close_to_failure));
  this->MaterialMesh.set_color(vertex, MeshGeometry::Color(red, green, blue));
}
