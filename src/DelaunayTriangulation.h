#ifndef __DelaunayTriangulation_h
#define __DelaunayTriangulation_h

#include "CrackDataTypes.h"
#include <vector>

void triangulate_points(std::vector<Vector3f> const& vertices,
                        MeshGeometry& output);

#endif
