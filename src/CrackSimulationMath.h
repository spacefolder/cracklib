#ifndef __CrackSimulationMath_h
#define __CrackSimulationMath_h

#include "CrackDataTypes.h"
#include <algorithm>

/** Get the angle between two vectors
 *
 * Compute and return the angle between two vectors in degrees.
 * Result will be between 0 and 180.
 *
 * Requirement: Vectors must be non-zero.
 *
 * Args:
 *   v1: First vector
 *   v2: Second vector
 *
 * Returns:
 *   Angle measured in degrees
 */

double angle_between(Vector3f const& v1, Vector3f const& v2);

/** Retrieve the largest eigenvalue and its corresponding evector
 *
 * Given a 3x3 matrix, find its eigendecomposition and return the
 * largest eigenvalue and its corresponding eigenvector.
 *
 * Args:
 *   matrix: 3x3 matrix to decompose
 *
 * Returns:
 *   EigenPair3 with the largest eigenvalue and vector
 */

EigenPair3 largest_eigenpair(Matrix3f const& matrix);

/** Construct an orthonormal basis from three vectors
 *
 * Given three non-collinear points, construct an orthonormal basis
 * for the plane spanned by those points.  This routine does not check
 * for co-linearity.  You're on your own there.
 *
 * Args:
 *   input_points: Array of 3 points
 *   output: Array where basis vectors will be stored
 *
 * Return value is stored in the 'output' arg.
 */

void make_orthonormal_basis(std::vector<Vector3f> const& input, std::vector<Vector3f>& output);

/** Project the corners of a triangle down into the plane
 *
 * NOTE: This function may need to be changed in case we need to save
 * the basis vectors.
 *
 * Given three vertices in 3-space, supposedly the vertices of a
 * triangle, construct an orthonormal basis for the plane spanned by
 * the triangle and project the three vertices down into it.
 *
 * Args:
 *  input_points: Vertices of the triangle (array of 3 points)
 *  output: Vertices projected down into 2d space
 */

void project_triangle_vertices_to_local_space(std::vector<Vector3f> const& vertices,
                                              std::vector<Vector2f>& output_coordinates);

/** Project a single vector down into a planar basis
 *
 */

Vector2f project_vector_into_triangle_space(Vector3f const& input_vector,
                                            std::vector<Vector3f> const& basis);

/** Construct a barycentric basis matrix for a triangle
 *
 * The matrix constructed by this function should project triangle
 * vertices from local coordinates into a space where their
 * coordinates add up to 1.
 *
 * Args:
 *  vertices: Array of 3 points in world space
 *
 * Returns:
 *  3x3 matrix with barycentric basis
 */

Matrix3f construct_barycentric_basis(std::vector<Vector3f> const& vertices);

/** Partition a 2x2 stress tensor into compressive and tensile components
 *
 * The action of a stress tensor upon an infinitesimal element can be
 * seen as a sum of two different forces: inward (compressive) and
 * outward (tensile).  The inward component is defined by any positive
 * eigenvalues of the tensor and their associated eigenvectors.  The
 * outward (tensile) component is defined by the negative eigenvalues
 * and their associated eigenvectors.
 *
 * We have to watch for cases where eigenvalues are repeated.  In this
 * case, any orthogonal frame will do.
 */

void decompose_stress_tensor(Matrix2f const& stress_tensor,
                             Matrix2f& compressive,
                             Matrix2f& tensile);



/** Construct a matrix with a desired eigenvector (2D version)
 *
 */

Matrix2f construct_matrix_with_desired_eigenvector(Vector2f const& evec);


/** Construct a matrix with a desired eigenvector (3D version)
 *
 */

Matrix3f construct_matrix_with_desired_eigenvector(Vector3f const& evec);

/** Check a container to make sure there are no NaNs
 */

template<typename container_type>
void ensure_no_nans(container_type const& thing)
{
#if !defined(NDEBUG)
  for (int r = 0; r < thing.rows(); ++r)
    {
    for (int c = 0; c < thing.cols(); ++c)
      {
      assert(!isnan(thing(r, c)));
      }
    }
#endif
}

/** Intersect a line with a plane
 *
 * We define a plane as the set of points (p - p0) . n = 0 where p0 is
 * a point on the plane and n is the plane normal.
 *
 * We can define a line as the set of points q = q0 + d * r where q0
 * is a point on the line, r is a direction vector and d is a real.
 *
 * This function computes the intersection of a line (line_begin + d *
 * line_direction) and a plane (p - plane_point) . plane_normal = 0.
 * It returns the constant d from the equation above.
 *
 * Args:
 *   plane_normal: Normal vector to the plane being tested
 *   plane_point: A point on the plane
 *   line_begin: A point on the line
 *   line_direction: Direction vector for the line
 *   solution_exists: Pointer to boolean that we will use to indicate whether a unique solution exists
 *
 * Returns:
 *   Multiplier d such that point of intersection = line_begin + d * line_direction.  If line and plane are parallel (do not intersect or line is entirely in plane) then *solution_exists will be set to false and the return value is undefined.
 */



double intersect_plane_with_line(Vector3f const& plane_point,
                                 Vector3f const& plane_normal,
                                 Vector3f const& line_begin,
                                 Vector3f const& line_direction,
                                 bool* solution_exists);


/** Compute the aspect ratio of a triangle
 *
 * Given three points A, B and C, the aspect ratio is the ratio
 * between the length of the longest side of the triangle and the
 * length of the shortest side.  The higher the aspect ratio, the more
 * unfriendly it is to try to do math with that triangle.
 */

double triangle_aspect_ratio(Vector3f const& A,
                             Vector3f const& B,
                             Vector3f const& c);

#endif
