#include "CrackDataTypes.h"
#include "CrackTriangleMesh.h"
#include "CrackSimulationMath.h"
#include "ExampleStressImages.h"
#include "GeometryFinders.h"

#include <Eigen/Dense>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>

#include <cmath>
#include <map>
#include <set>

#include <boost/random.hpp>


namespace {

template<typename T>
T const& wraparound_access(std::vector<T> const& stuff, std::size_t i)
{
  return stuff[i % stuff.size()];
}

template<typename T>
std::size_t index_of(T const& key, std::vector<T> const& stuff)
{
  for (std::size_t here = 0; here < stuff.size(); ++here)
    {
    if (stuff[here] == key)
      return here;
    }
  std::cout << "ERROR: Couldn't find item in vector!\n";
  assert(1==0);
}

typedef boost::random::mt19937 base_generator_type;
base_generator_type random_generator(12345);

double uniform_random(double lower_limit, double upper_limit)
{
  boost::uniform_real<> dist(lower_limit, upper_limit);
  return dist(random_generator);
}

template<typename om_vector_type>
Vector3f vector3_om_to_eigen(om_vector_type const& in)
{
  return Vector3f(in[0], in[1], in[2]);
}

MeshGeometry::Point vector3_eigen_to_om(Vector3f const& v)
{
  return MeshGeometry::Point(v(0), v(1), v(2));
}

std::string halfedge_as_string(HalfedgeHandle he, MeshGeometry const& m)
{
  std::ostringstream outbuf;
  outbuf << he << " ("
         << m.from_vertex_handle(he)
         << " -> "
         << m.to_vertex_handle(he)
         << ")";
  return outbuf.str();
}

class FaceAction
{
public:
  FaceHandle FaceToDelete;
  std::vector<VertexHandle> FaceToAdd;
  Matrix2f SavedStressTensor;
};

}

// ----------------------------------------------------------------------

CrackTriangleMesh::CrackTriangleMesh()
{
  this->CurrentIteration = 0;
  this->SimulationTime = 0;
  this->CurrentTimeStep = -1;
  this->CrackEdgeSnapThreshold = 25;
  this->InteriorEdgeSnapThreshold = 15;
  this->MaterialHomogeneity = 0.9;
  this->NumRelaxStepsPerIteration = 10;
  this->Toughness = 1;
  this->Verbosity = 0;
  this->TopologyHasChanged = true;
}

// ----------------------------------------------------------------------

CrackTriangleMesh::CrackTriangleMesh(MeshGeometry const& initial_geometry)
{
  this->CurrentIteration = 0;
  this->SimulationTime = 0;
  this->CurrentTimeStep = -1;
  this->CrackEdgeSnapThreshold = 25;
  this->InteriorEdgeSnapThreshold = 15;
  this->MaterialHomogeneity = 0.25;
  this->NumRelaxStepsPerIteration = 100;
  this->Toughness = 1;
  this->Verbosity = 0;
  this->TopologyHasChanged = true;

  this->set_initial_geometry(initial_geometry);
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::acknowledge_topology_change()
{
  this->TopologyHasChanged = false;
}

// ----------------------------------------------------------------------

FaceHandle
CrackTriangleMesh::add_face(std::vector<VertexHandle> const& vertices)
{
  FaceHandle face = this->MaterialMesh.add_face(vertices);
  assert(this->MaterialMesh.valence(face) == 3);
  this->initialize_face_property_values(face);

  return face;
}

// ----------------------------------------------------------------------

FaceHandle
CrackTriangleMesh::add_face_to_crack_mesh(FaceHandle const& face)
{
  std::vector<VertexHandle> vertices;
  for (MeshGeometry::ConstFaceVertexIter v_iter = this->MaterialMesh.cfv_iter(face);
       v_iter.is_valid();
       ++v_iter)
    {
    vertices.push_back(*v_iter);
    }

  return this->CrackMesh.add_face(vertices);
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::add_uniform_contraction(float amount)
{
  for (face_iterator iter = this->MaterialMesh.faces_sbegin();
       iter != this->MaterialMesh.faces_end();
       ++iter)
    {
    if (this->MaterialMesh.property(this->FaceIsIsolated, *iter))
      {
      continue; // don't touch isolated faces
      }
    
    this->MaterialMesh.property(this->StressTensor, *iter) += amount * Matrix2f::Identity();
    ensure_no_nans(this->MaterialMesh.property(this->StressTensor, *iter));
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::add_random_contraction(float max_amount)
{
  for (face_iterator iter = this->MaterialMesh.faces_sbegin();
       iter != this->MaterialMesh.faces_end();
       ++iter)
    {
    double randomness = ::uniform_random(0, max_amount);
    this->MaterialMesh.property(this->StressTensor, *iter) +=  randomness * Matrix2f::Identity();
    ensure_no_nans(this->MaterialMesh.property(this->StressTensor, *iter));
    }
}

// ----------------------------------------------------------------------

VertexHandle
CrackTriangleMesh::add_vertex(MeshGeometry::Point const& position)
{
  VertexHandle handle = this->MaterialMesh.add_vertex(position);
  this->CrackMesh.add_vertex(position);
  this->initialize_vertex_property_values(handle);
  this->MaterialMesh.property(this->MaterialVertexSimulationPosition, handle) = ::vector3_om_to_eigen(position);
  return handle;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::add_residual_separation_tensor(VertexHandle const& source_vertex,
                                                  VertexHandle const& destination_vertex,
                                                  Vector3f const& crack_plane_normal)
{
  float residual_stress =
    this->MaterialMesh.property(this->LargestSeparationEigenvalue, source_vertex) -
    this->MaterialMesh.property(this->MaterialToughness, source_vertex);

  Matrix3f adjustment =
    this->MaterialHomogeneity *
    residual_stress *
    construct_matrix_with_desired_eigenvector(crack_plane_normal);

  this->MaterialMesh.property(this->SeparationTensorResidual, destination_vertex) += adjustment;
}

// ----------------------------------------------------------------------'

void
CrackTriangleMesh::advance_simulation(float how_long)
{
  if ((this->Verbosity > 1 && this->CurrentIteration % 100 == 0) || this->CurrentIteration % 1000 == 0)
    {
    std::cout << "********** Beginning iteration " << this->CurrentIteration << "\n";
    }

  this->sanity_check_faces();

  this->CurrentTimeStep = how_long;

  if (this->Verbosity > 20)
    {
    std::cout << "Before iteration " << this->CurrentIteration << ":\n";
    this->debug_print_mesh_topology();
    }

  this->relax_stress_field();
  this->clear_separation_tensors();
  this->identify_disconnected_faces();
  this->distribute_stress_into_separation_tensors();
  this->enqueue_failing_vertices();
  this->introduce_new_cracks();

  ++this->CurrentIteration;
  this->SimulationTime += how_long;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::clear_forces_on_vertex(MeshGeometry::VertexHandle const& v_handle)
{
  this->MaterialMesh.property(this->TotalCompressiveForce, v_handle) = Vector3f::Zero();
  this->MaterialMesh.property(this->TotalTensileForce, v_handle) = Vector3f::Zero();
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::clear_separation_tensors()
{
  this->TotalSeparationStress = 0;
  
  Vector3f zero_vector(0, 0, 0);

  for (vertex_iterator vertex_iter = this->MaterialMesh.vertices_sbegin();
       vertex_iter != this->MaterialMesh.vertices_end();
       ++vertex_iter)
    {
    this->MaterialMesh.property(this->TotalCompressiveForce, *vertex_iter) = zero_vector;
    this->MaterialMesh.property(this->TotalTensileForce, *vertex_iter) = zero_vector;
    this->MaterialMesh.property(this->SeparationTensor, *vertex_iter) = Matrix3f::Zero();
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::compute_barycentric_basis()
{
  for (face_iterator face_iter = this->MaterialMesh.faces_sbegin();
       face_iter != this->MaterialMesh.faces_end();
       ++face_iter)
    {
    this->compute_barycentric_basis(*face_iter);
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::compute_barycentric_basis(FaceHandle const& face)
{
  assert(this->MaterialMesh.valence(face) == 3);

  std::vector<Vector3f> world_vertices;
  this->get_face_vertex_positions(face, world_vertices);

  this->MaterialMesh.property(this->BarycentricBasisMatrix, face) =
    construct_barycentric_basis(world_vertices);

//  ensure_no_nans(this->MaterialMesh.property(this->BarycentricBasisMatrix, face));
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::compute_face_area(FaceHandle const& face)
{
  assert(this->MaterialMesh.valence(face) == 3);
  std::vector<Vector3f> vertices;

  this->get_face_vertex_positions(face, vertices);

  // Heron's formula!
  double distances[3]; // distance AB, AC, BC
  distances[0] = (vertices[1] - vertices[0]).norm();
  distances[1] = (vertices[2] - vertices[0]).norm();
  distances[2] = (vertices[2] - vertices[1]).norm();
  double semiperimeter = 0.5 * (distances[0] + distances[1] + distances[2]);

  double area = sqrt(
    semiperimeter *
    (semiperimeter - distances[0]) *
    (semiperimeter - distances[1]) *
    (semiperimeter - distances[2])
    );

  assert(!isnan(area));
  this->MaterialMesh.property(this->FaceArea, face) = area;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::compute_face_areas()
{
  for (face_iterator face_iter = this->MaterialMesh.faces_sbegin();
       face_iter != this->MaterialMesh.faces_end();
       ++face_iter)
    {
    this->compute_face_area(*face_iter);
    }
}

// ----------------------------------------------------------------------

Vector3f
CrackTriangleMesh::compute_force_on_vertex(
  VertexHandle const& vertex,
  int vertex_id,
  std::vector<Vector3f> const& vertex_coordinates,
  double face_area,
  Matrix3f const& beta,
  Matrix2f const& stress_tensor,
  bool is_compressive
  ) const
{
  std::vector<Vector3f> const& p(vertex_coordinates);

  // We're going to use I, J, K and L as our loop indices so that we
  // can reproduce Equation 1 from Ilen and O'Brien as accurately as
  // we can.  Note that beta is the barycentric basis matrix and p
  // will be the world-space vertex positions.
  Vector3f result(0, 0, 0);

  int I = vertex_id;
  for (int J = 0; J < 3; ++J)
    {
    double scale_factor = 0;
    for (int K = 0; K < 2; ++K)
      {
      for (int L = 0; L < 2; ++L)
        {
        scale_factor +=
          beta(J, L) * beta(I, K) * stress_tensor(K, L);
        }
      }
    assert(scale_factor < 10000000);
    result += p[J] * scale_factor;
    }

  result = -face_area * result;

  ensure_no_nans(result);
  return result;
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::compute_stress_tensor_derivatives()
{
  for (MeshGeometry::FaceIter face_iter = this->MaterialMesh.faces_sbegin();
       face_iter != this->MaterialMesh.faces_end();
       ++face_iter)
    {
    this->compute_stress_tensor_derivatives(*face_iter);
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::compute_stress_tensor_derivatives(FaceHandle const& face)
{
  assert(this->MaterialMesh.valence(face) == 3);
  assert(this->MaterialMesh.status(face).deleted() == false);

  int vertex_id = 0;

  this->sanity_check_faces();

  Matrix3f const& barycentric_basis(
    this->MaterialMesh.property(this->BarycentricBasisMatrix, face)
    );

  std::vector<Vector3f> vertex_positions;
  this->get_face_vertex_positions(face, vertex_positions);

  for (MeshGeometry::ConstFaceVertexIter vertex_iter =
         this->MaterialMesh.cfv_iter(face);
       vertex_iter.is_valid();
       ++vertex_iter, ++vertex_id)
    {
    if (vertex_id == 3)
      {
      std::cout << "*** ERROR: Found a face with more than 3 vertices!  Mesh topology:\n";
      this->debug_print_mesh_topology();
      assert(1==0);
      }

    this->compute_stress_tensor_derivatives_at_vertex(
      face,
      vertex_id,
      vertex_positions,
      barycentric_basis
      );
    }

}

// ----------------------------------------------------------------------

// Compute the derivative of the stress tensor on a given face with
// respect to one particular vertex of that face.  Oddly enough, this
// is independent of the values of the stress tensor.


void CrackTriangleMesh::compute_stress_tensor_derivatives_at_vertex(
  FaceHandle const& face,
  int vertex_id,
  std::vector<Vector3f> const& vertex_positions,
  Matrix3f const& barycentric_basis
  )
{
  assert(this->MaterialMesh.valence(face) == 3);

  // We are computing the stress tensor derivatives for vertex
  // "vertex_id" of the current face.  They will go in
  // this->StressTensorDerivatives[vertex_id][0, 1, 2] for components
  // 0, 1, and 2 of the vertex, respectively.

  int n = vertex_id;
  Matrix2f derivatives[3];

  for (int r = 0; r < 3; ++r)
    {
    derivatives[r] = Matrix2f::Zero();

    for (int i = 0; i < 2; ++i)
      {
      for (int j = 0; j < 2; ++j)
        {
        for (int m = 0; m < 3; ++m)
          {
          derivatives[r](i, j) +=
            vertex_positions[m](r) *
            barycentric_basis(n, i) *
            barycentric_basis(m, j);

          derivatives[r](i, j) +=
            vertex_positions[m](r) *
            barycentric_basis(m, i) *
            barycentric_basis(n, j);
          }
        derivatives[r](i, j) *= 0.5;
        }
      }
//    ensure_no_nans(derivatives[r]);
    this->MaterialMesh.property(this->StressTensorDerivatives[n][r], face) = derivatives[r];
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::compute_all_vertex_displacements(float timestep)
{
  for (MeshGeometry::ConstVertexIter v_iter = this->MaterialMesh.vertices_sbegin();
       v_iter != this->MaterialMesh.vertices_end();
       ++v_iter)
    {
    this->MaterialMesh.property(this->LatestVertexDisplacement, *v_iter) =
      this->compute_virtual_displacement(*v_iter, timestep);
    }
}

// ----------------------------------------------------------------------

Vector3f
CrackTriangleMesh::compute_virtual_displacement(VertexHandle const& vertex, float time_step) const
{
  Vector3f force_on_vertex(this->total_force_on_vertex(vertex));
  ensure_no_nans(force_on_vertex);


  if (this->Verbosity > 7)
    {
    if (force_on_vertex != Vector3f::Zero())
      {
      std::cout << "DEBUG: virtual displacement: Vertex at ("
                << this->vertex_simulation_position(vertex).transpose() << ") should move by ("
                << (time_step * force_on_vertex).transpose() << ")\n";
      }
    }

  return time_step * force_on_vertex;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::copy_face_properties(FaceHandle const& f_from, FaceHandle const& f_to)
{
  this->initialize_face_property_values(f_to);

#define F_COPY_PROPERTY(PROP_NAME) this->MaterialMesh.property(this->PROP_NAME, f_to) = this->MaterialMesh.property(this->PROP_NAME, f_from)

  F_COPY_PROPERTY(StressTensor);

#undef F_COPY_PROPERTY
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::copy_vertex_properties(VertexHandle const& v_from, VertexHandle const& v_to)
{
#define V_COPY_PROPERTY(PROP_NAME) this->MaterialMesh.property(this->PROP_NAME, v_to) = this->MaterialMesh.property(this->PROP_NAME, v_from)

  V_COPY_PROPERTY(LargestSeparationEigenvalue);
  V_COPY_PROPERTY(LatestVertexDisplacement);
  V_COPY_PROPERTY(MaterialToughness);
  V_COPY_PROPERTY(SeparationTensor);
  V_COPY_PROPERTY(SeparationTensorResidual);
  V_COPY_PROPERTY(TotalCompressiveForce);
  V_COPY_PROPERTY(TotalTensileForce);
  V_COPY_PROPERTY(VertexBelongsToIsolatedFace);
  V_COPY_PROPERTY(VertexIsCrackTip);
  V_COPY_PROPERTY(VertexIsFixed);
  V_COPY_PROPERTY(VertexIsOnCrackBoundary);
  V_COPY_PROPERTY(MaterialVertexSimulationPosition);

#undef V_COPY_PROPERTY
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_mesh_topology()
{
  this->debug_print_vertices();
  this->debug_print_halfedges();
  this->debug_print_edges();
  this->debug_print_faces();
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_vertices()
{
  std::cout << "Mesh has " << this->MaterialMesh.n_vertices() << " vertices:\n";
  for (MeshGeometry::ConstVertexIter v_iter = this->MaterialMesh.vertices_sbegin();
       v_iter != this->MaterialMesh.vertices_end();
       ++v_iter)
    {
    std::cout << "  ";
    this->debug_print_vertex(*v_iter);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_vertex(VertexHandle const& v) const
{
  std::cout << "Vertex " << v << ": cosmetic position ("
            << this->MaterialMesh.point(v) << "), simulation position ("
            << this->MaterialMesh.property(this->MaterialVertexSimulationPosition, v).transpose() << ")";

  if (this->MaterialMesh.property(this->VertexIsOnCrackBoundary, v))
    {
    std::cout << ", borders crack";
    }
  if (this->MaterialMesh.property(this->VertexIsCrackTip, v))
    {
    std::cout << ", is crack tip";
    }
  std::cout << "\n";
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_halfedges()
{
  std::cout << "Mesh has " << 2 * this->MaterialMesh.n_edges() << " halfedges:\n";
  for (MeshGeometry::ConstHalfedgeIter h_iter = this->MaterialMesh.halfedges_begin();
       h_iter != this->MaterialMesh.halfedges_end();
       ++h_iter)
    {
    std::cout << "  ";
    this->debug_print_halfedge(*h_iter);
    }
}

// ----------------------------------------------------------------------


void
CrackTriangleMesh::debug_print_halfedge(HalfedgeHandle const& he) const
{
  std::cout << "Halfedge " << he << ": from "
            << this->MaterialMesh.from_vertex_handle(he)
            << " to "
            << this->MaterialMesh.to_vertex_handle(he)
            << ", face handle "
            << this->MaterialMesh.face_handle(he);

    if (this->MaterialMesh.is_boundary(he))
      {
      std::cout << ", on material boundary";
      }
    std::cout << "\n";
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_edges()
{
  std::cout << "Mesh has " << this->MaterialMesh.n_edges() << " edges:\n";

  for (MeshGeometry::ConstEdgeIter e_iter = this->MaterialMesh.edges_sbegin();
       e_iter != this->MaterialMesh.edges_end();
       ++e_iter)
    {
    std::cout << "  ";
    this->debug_print_edge(*e_iter);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_edge(EdgeHandle const& e) const
{
  std::cout << "Edge " << e << ": "
            << "halfedges "
            << this->MaterialMesh.halfedge_handle(e, 0) << ", "
            << this->MaterialMesh.halfedge_handle(e, 1);

  if (this->MaterialMesh.is_boundary(e))
    {
    std::cout << ", on material boundary";
    }
  if (this->edge_is_crack_boundary(e))
    {
    std::cout << ", on crack boundary";
    }
  std::cout << "\n";
}

// ----------------------------------------------------------------------


void
CrackTriangleMesh::debug_print_faces()
{
  std::cout << "Mesh has " << this->MaterialMesh.n_faces() << " faces:\n";

  for (MeshGeometry::ConstFaceIter f_iter = this->MaterialMesh.faces_sbegin();
       f_iter != this->MaterialMesh.faces_end();
       ++f_iter)
    {
    std::cout << "  Face " << *f_iter << ": "
              << "vertex handles";

    for (MeshGeometry::ConstFaceVertexIter v_iter = this->MaterialMesh.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      std::cout << " " << *v_iter;
      }

    std::cout << ", vertex positions";

    for (MeshGeometry::ConstFaceVertexIter v_iter = this->MaterialMesh.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      std::cout << " (" << this->MaterialMesh.point(*v_iter) << ")";
      }
    std::cout << "\n";
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_vertex_properties(VertexHandle const& vertex)
{
  Vector3f position(this->vertex_simulation_position(vertex));
  Vector3f compression(this->MaterialMesh.property(this->TotalCompressiveForce, vertex));
  Vector3f tension(this->MaterialMesh.property(this->TotalTensileForce, vertex));

  if (this->Verbose || (compression != Vector3f::Zero() || tension != Vector3f::Zero()))
    {
    std::cout << "DEBUG: Vertex at ("
              << position.transpose()
              << ") (cosmetic position ("
              << this->MaterialMesh.point(vertex) << "))"
              << ": compressive force "
              << compression.transpose()
              << ", tensile force "
              << tension.transpose()
              << ", separation eigenvalue "
              << this->MaterialMesh.property(this->LargestSeparationEigenvalue, vertex) << "\n";
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_face_properties(FaceHandle const& face)
{
  std::vector<Vector3f> vertices;
  Matrix2f stress_tensor(this->MaterialMesh.property(this->StressTensor, face));
  float area(this->MaterialMesh.property(this->FaceArea, face));

  if (stress_tensor != Matrix2f::Zero())
    {
    this->get_face_vertex_positions(face, vertices);

    std::cout << "DEBUG: Face with vertices "
              << "(" << vertices[0].transpose() << "), "
              << "(" << vertices[1].transpose() << "), "
              << "(" << vertices[2].transpose() << "):\n";
    std::cout << "Face area: " << area << "\n";
    std::cout << "Stress tensor:\n"
              << stress_tensor << "\n";
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_mesh_properties()
{
  std::cout << "\n\nDEBUG INFORMATION after iteration "
            << this->CurrentIteration
            << ", clock time "
            << this->SimulationTime << "\n";

  for (vertex_iterator vit = this->MaterialMesh.vertices_sbegin();
       vit != this->MaterialMesh.vertices_end();
       ++vit)
    {
    this->debug_print_vertex_properties(*vit);
    }

  for (face_iterator fit = this->MaterialMesh.faces_sbegin();
       fit != this->MaterialMesh.faces_end();
       ++fit)
    {
    this->debug_print_face_properties(*fit);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_set_single_triangle_mesh()
{
  MeshGeometry initial_mesh;
  std::vector<MeshGeometry::VertexHandle> vertices;
  std::vector<MeshGeometry::VertexHandle> face;

  vertices.push_back(initial_mesh.add_vertex(MeshGeometry::Point(0, 0, 0)));
  vertices.push_back(initial_mesh.add_vertex(MeshGeometry::Point(1, 0, 0)));
  vertices.push_back(initial_mesh.add_vertex(MeshGeometry::Point(0, 1, 0)));

  face.push_back(vertices[0]);
  face.push_back(vertices[1]);
  face.push_back(vertices[2]);

  initial_mesh.add_face(face);
  face.clear();

  this->fix_boundary_edges();
  this->set_initial_geometry(initial_mesh);
  this->set_material_toughness(10000000);
  this->add_uniform_contraction(1);
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_print_separation_eigenvalues()
{
  double total_evalues = 0;
  int total_vertices = 0;
  double max_evalue = -1000000;
  double min_evalue = 10000000;

  std::cout << "Stats for largest separation eigenvalue:\n";
  for (MeshGeometry::VertexIter v_iter = this->MaterialMesh.vertices_sbegin();
       v_iter != this->MaterialMesh.vertices_end();
       ++v_iter)
    {
    float evalue = this->MaterialMesh.property(this->LargestSeparationEigenvalue, *v_iter);
    total_evalues += evalue;
    ++total_vertices;
    if (evalue > max_evalue) max_evalue = evalue;
    if (evalue < min_evalue) min_evalue = evalue;

    std::cout << evalue << "\n";
    }

  std::cout << "Average eigenvalue: " << total_evalues / total_vertices << "\n";
  std::cout << "Largest: " << max_evalue << " Smallest: " << min_evalue << " Total: " << total_evalues << "\n";

}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_set_rectangular_triangle_mesh(int rows, int cols,
                                                       bool add_high_stress_to_corner)
{
  MeshGeometry initial_mesh;
  std::vector<MeshGeometry::VertexHandle> vertices;
  std::vector<MeshGeometry::FaceHandle> faces;

  float row_step = 1.0 / (rows - 1);
  float col_step = 1.0 / (cols - 1);

  row_step = 1;
  col_step = 1;

  float row_coord, col_coord;

  for (int r = 0; r < rows; ++r)
    {
    row_coord = row_step * r;
    for (int c = 0; c < cols; ++c)
      {
      col_coord = c * col_step;

      vertices.push_back(initial_mesh.add_vertex(MeshGeometry::Point(col_coord, row_coord, 0)));
      }
    }

  std::vector<int> high_stress_face_ids;

  std::vector<MeshGeometry::VertexHandle> face;
  for (int r = 0; r < rows-1; ++r)
    {
    for (int c = 0; c < cols-1; ++c)
      {
      face.push_back(vertices[r*cols + c]);
      face.push_back(vertices[r*cols + (c+1)]);
      face.push_back(vertices[(r+1)*cols + c]);
      faces.push_back(initial_mesh.add_face(face));
      face.clear();

      face.push_back(vertices[r*cols + (c+1)]);
      face.push_back(vertices[(r+1)*cols + (c+1)]);
      face.push_back(vertices[(r+1)*cols + c]);
      faces.push_back(initial_mesh.add_face(face));
      face.clear();

      if ((r == 0 && c == 0) ||
          (r == rows-1 && c == cols-1))
        {
        high_stress_face_ids.push_back(faces.size() - 2);
        high_stress_face_ids.push_back(faces.size() - 1);
        }
      }
    }

  this->set_initial_geometry(initial_mesh);
  this->fix_boundary_edges();

  Matrix2f high_stress = Matrix2f::Identity();
  if (add_high_stress_to_corner)
    {
    for (std::vector<int>::const_iterator iter = high_stress_face_ids.begin();
         iter != high_stress_face_ids.end();
         ++iter)
      {
      this->MaterialMesh.property(
        this->StressTensor, this->MaterialMesh.face_handle(*iter)
        ) = high_stress;

      std::vector<Vector3f> vertices;
      this->get_face_vertex_positions(this->MaterialMesh.face_handle(*iter), vertices);
      std::cout << "Setting high stress on face with vertices "
                << vertices[0].transpose() << ", " << vertices[1].transpose()
                << ", " << vertices[2].transpose() << " \n";

      }
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::debug_write_separation_image(int rows, int cols, const char* filename, float maxval)
{

  float scale = 65535;

  std::ofstream outfile(filename);
  outfile << "P2\n" << cols << " " << rows << " " << static_cast<int>(scale) << "\n";

  float max_value = 0;
  float min_value = 1000000000;

  std::cout << "write_separation_image: n_vertices = " << this->MaterialMesh.n_vertices() << "\n";

  for (MeshGeometry::VertexIter v_it = this->MaterialMesh.vertices_sbegin();
       v_it != this->MaterialMesh.vertices_end();
       ++v_it)
    {
    float evalue = this->MaterialMesh.property(this->LargestSeparationEigenvalue, *v_it);
    if (evalue != 0)
      {
      float log_evalue = log(this->MaterialMesh.property(this->LargestSeparationEigenvalue, *v_it));
      if (log_evalue > max_value) max_value = log_evalue;
      if (log_evalue < min_value) min_value = log_evalue;
      }
    }

  std::cout << "evalue min, max: " << min_value << ", " << max_value << "\n";

  assert(!isnan(min_value));
  assert(!isnan(max_value));

  int num_zeros;
  for (int r = 0; r < rows; ++r)
    {
    for (int c = 0; c < cols; ++c)
      {
      float evalue = this->MaterialMesh.property(
          this->LargestSeparationEigenvalue,
          this->MaterialMesh.vertex_handle(r*cols + c)
        );

      float log_evalue = min_value;
      if (evalue > 0)
        {
        log_evalue = log(evalue);
        }
      log_evalue = (log_evalue - min_value) / (max_value - min_value);

      float f_gray_level = scale * log_evalue;

#if 0
      if (f_gray_level == 0)
        {
        ++num_zeros;
        }
      else
        {
        std::cout << "f_gray_level: " << f_gray_level << "\n";
        }
#endif
      if (f_gray_level < 0) f_gray_level = 0;
      if (f_gray_level > scale) f_gray_level = scale;
      int i_gray_level = static_cast<int>(f_gray_level);
      outfile << i_gray_level << " ";
      }
    outfile << "\n";
    }
  std::cout << "write_separation_image, iteration " << this->CurrentIteration << ": " << num_zeros << " zeros\n";
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::disconnect_perforation_corner(HalfedgePair const& corner)
{
  EdgeHandle edge1(this->MaterialMesh.edge_handle(corner.first));
  EdgeHandle edge2(this->MaterialMesh.edge_handle(corner.second));

  if (this->MaterialMesh.property(this->EdgeIsFixed, edge1) ||
      this->MaterialMesh.property(this->EdgeIsFixed, edge2))
    {
    std::cout << "PERF: Ignoring perforation vertex "
              << this->MaterialMesh.to_vertex_handle(corner.first)
              << " because it is on a fixed edge.\n";
    }
}

// ----------------------------------------------------------------------

// A 'perforation corner' is a face that is surrounded on two sides by
// boundary edges but is connected at the vertex between those edges.
// We want to disconnect that vertex so that the mesh starts behaving
// better.

void
CrackTriangleMesh::disconnect_perforation_corners()
{
  std::vector<HalfedgePair> perf_corners;

  this->find_perforation_corners(perf_corners);

  for (std::vector<HalfedgePair>::const_iterator iter = perf_corners.begin();
       iter != perf_corners.end();
       ++iter)
    {
    this->disconnect_perforation_corner(*iter);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::distribute_stress_into_separation_tensors()
{
  for (face_iterator face_iter = this->MaterialMesh.faces_sbegin();
       face_iter != this->MaterialMesh.faces_end();
       ++face_iter)
    {
    if (this->MaterialMesh.property(this->FaceIsIsolated, *face_iter) == false)
      {
      this->distribute_stress_from_face(*face_iter);
      }
    }

  for (edge_iterator edge_iter = this->MaterialMesh.edges_sbegin();
       edge_iter != this->MaterialMesh.edges_end();
       ++edge_iter)
    {
    if (this->MaterialMesh.property(this->EdgeIsFixed, *edge_iter))
      {
      MeshGeometry::VertexHandle v_from(
        this->MaterialMesh.to_vertex_handle(
          this->MaterialMesh.halfedge_handle(*edge_iter, 0)
          )
        );
      MeshGeometry::VertexHandle v_to(
        this->MaterialMesh.from_vertex_handle(
          this->MaterialMesh.halfedge_handle(*edge_iter, 0)
          )
        );

      this->clear_forces_on_vertex(v_from);
      this->clear_forces_on_vertex(v_to);
      }
    }

  for (vertex_iterator vertex_iter = this->MaterialMesh.vertices_sbegin();
       vertex_iter != this->MaterialMesh.vertices_end();
       ++vertex_iter)
    {
    this->finish_computing_separation_tensor(*vertex_iter);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::distribute_stress_from_face(FaceHandle const& face)
{
  assert(this->MaterialMesh.valence(face) == 3);

  // We need these values every time we visit a vertex so we might as
  // well just grab them once
  Matrix3f& barycentric_basis(
    this->MaterialMesh.property(this->BarycentricBasisMatrix, face)
    );

  std::vector<Vector3f> vertex_coords;
  this->get_face_vertex_positions(face, vertex_coords);

  double face_area = this->MaterialMesh.property(this->FaceArea, face);

  Matrix2f compressive_stress, tensile_stress;
  decompose_stress_tensor(
    this->MaterialMesh.property(this->StressTensor, face),
    compressive_stress,
    tensile_stress
    );

  ensure_no_nans(compressive_stress);
  ensure_no_nans(tensile_stress);

  int vertex_id = 0;
  MeshGeometry::ConstFaceVertexIter vertex_iter = this->MaterialMesh.cfv_iter(face);

  for (; vertex_iter.is_valid(); ++vertex_id, ++vertex_iter)
    {
    if (this->MaterialMesh.property(this->VertexIsFixed, *vertex_iter))
      continue; // no force on fixed vertices

    Vector3f tensile_force = this->compute_force_on_vertex(
      *vertex_iter,
      vertex_id,
      vertex_coords,
      face_area,
      barycentric_basis,
      tensile_stress,
      false
      );

    ensure_no_nans(tensile_force);

    this->MaterialMesh.property(this->TotalTensileForce, *vertex_iter) += tensile_force;
    this->MaterialMesh.property(this->SeparationTensor, *vertex_iter) += construct_matrix_with_desired_eigenvector(tensile_force);

    Vector3f compressive_force = this->compute_force_on_vertex(
      *vertex_iter,
      vertex_id,
      vertex_coords,
      face_area,
      barycentric_basis,
      compressive_stress,
      true
      );

    ensure_no_nans(compressive_force);

    assert(compressive_force.norm() < 1000000000);

    this->MaterialMesh.property(this->TotalCompressiveForce, *vertex_iter) += compressive_force;
    this->MaterialMesh.property(this->SeparationTensor, *vertex_iter) -= construct_matrix_with_desired_eigenvector(compressive_force);
    }

  assert(vertex_id == 3);
}

// ----------------------------------------------------------------------

VertexHandle
CrackTriangleMesh::duplicate_vertex(VertexHandle const& which)
{
  MeshGeometry::Point new_position(this->MaterialMesh.point(which));
  if (this->Verbosity > 5)
    {
    std::cout << "duplicate_vertex: Copying vertex "
              << which << " at position ("
              << new_position << ")\n";
    }

  new_position[2] -= 0.01;
  VertexHandle copy = this->MaterialMesh.add_vertex(new_position);
  this->CrackMesh.add_vertex(new_position);

  this->copy_vertex_properties(which, copy);
  return copy;
}

// ----------------------------------------------------------------------

bool
CrackTriangleMesh::edge_is_crack_boundary(EdgeHandle const& edge) const
{
  assert(edge.is_valid());
  return (
    this->vertex_is_on_crack_boundary(
      this->MaterialMesh.from_vertex_handle(
        this->MaterialMesh.halfedge_handle(edge, 0)
        ))
    &&
    this->vertex_is_on_crack_boundary(
      this->MaterialMesh.to_vertex_handle(
        this->MaterialMesh.halfedge_handle(edge, 0)
        ))
    );
}

// ----------------------------------------------------------------------

bool
CrackTriangleMesh::edge_is_crack_boundary(HalfedgeHandle const& he) const
{
  assert(he.is_valid());

  VertexHandle v_to = this->MaterialMesh.to_vertex_handle(he);
  VertexHandle v_from = this->MaterialMesh.from_vertex_handle(he);

  return (
    this->MaterialMesh.property(this->VertexIsOnCrackBoundary, v_to) &&
    this->MaterialMesh.property(this->VertexIsOnCrackBoundary, v_from) &&
    this->MaterialMesh.is_boundary(he)
    );
}

// ----------------------------------------------------------------------

bool
CrackTriangleMesh::edge_is_crack_boundary(VertexHandle const& v1, VertexHandle const& v2) const
{
  assert(v1.is_valid());
  assert(v2.is_valid());
  return (
    this->edge_is_crack_boundary(this->MaterialMesh.find_halfedge(v1, v2)) ||
    this->edge_is_crack_boundary(this->MaterialMesh.find_halfedge(v2, v1))
    );
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::emplace_crack_vertices(std::vector<CrackVertex> & new_vertices)
{
  for (std::vector<CrackVertex>::iterator iter = new_vertices.begin();
       iter != new_vertices.end();
       ++iter)
    {
    this->snap_crack_to_nearby_edges(*iter);
    assert(iter->Face.is_valid());

    if (this->MaterialMesh.property(this->VertexIsCrackTip, iter->OriginVertex))
      {
      this->mark_crack_tip(iter->OriginVertex, false);
      }
    this->mark_vertex_on_crack_boundary(iter->OriginVertex, true);

    if (iter->VertexIsNew)
      {
      this->emplace_new_crack_vertex(*iter);
      }
    else
      {
      // If the vertex already exists and is connected to the crack
      // origin by a crack boundary edge then it cannot become a
      // crack tip.
      if (this->edge_is_crack_boundary(iter->CrackVertexHandle, iter->OriginVertex) == false)
        {
        this->mark_crack_tip(iter->CrackVertexHandle, true);
        }

      this->add_residual_separation_tensor(iter->OriginVertex,
                                           iter->CrackVertexHandle,
                                           iter->CrackPlaneNormal);
      }
    }
}


// ----------------------------------------------------------------------

void
CrackTriangleMesh::emplace_new_crack_vertex(CrackVertex& new_vertex)
{
  // The tricky part about this operation is making sure the stress
  // tensor gets copied to the new faces that get created.
  //
  // We have a setup like the following:
  //
  //           P
  //          / \
  //         /   \    far face
  //        /     \
  //        *--N--*
  //        \     /
  //         \   /    near face
  //          \ /
  //           C
  //
  // where C is the crack origin and N is the new vertex that we're
  // about to insert.  We call the upper triangle the 'far face' and
  // the lower triangle the 'near face'.
  //
  // When we call split_copy() on the middle edge, OpenMesh is going
  // to split the near and far faces without giving us handles to the
  // new geometry.  The only way we can identify them is by looking
  // for faces adjacent to vertex N that have either (N, P) or (N, C)
  // as an edge.  However, once we do so, we can copy in the
  // appropriate stress tensor.

  assert(new_vertex.Face.is_valid());

  MeshGeometry::Point crack_center_position(
    new_vertex.CrackVertexPosition(0),
    new_vertex.CrackVertexPosition(1),
    new_vertex.CrackVertexPosition(2)
    );

  VertexHandle new_crack_tip = this->add_vertex(crack_center_position);
  // YOU ARE HERE
  new_vertex.CrackVertexHandle = new_crack_tip;
  this->initialize_vertex_property_values(new_crack_tip);
  this->mark_crack_tip(new_crack_tip, true);
  this->MaterialMesh.property(this->MaterialVertexSimulationPosition, new_crack_tip) = ::vector3_om_to_eigen(crack_center_position);


  if (this->Verbosity > 3)
    {
    std::cout << "New crack tip vertex " << new_crack_tip << " at position (" << crack_center_position << ")\n";
    }

  // We're about to split this vertex.  It can no longer be a crack tip.
  this->mark_crack_tip(new_vertex.OriginVertex, false);

  // Now some bookkeeping.  We're going to split one or two faces.
  // After we do that we will need to copy the stress tensors back
  // into those faces.
  FaceHandle near_face(new_vertex.Face);
  Matrix2f near_stress_tensor(this->stress_tensor(near_face));


  assert(near_face.is_valid());
  if (this->Verbosity > 5)
    {
    std::cout << "DEBUG: Face adjacent to crack has coordinates "
              << this->face_coordinates_as_string(near_face)
              << "\n";
    }

  bool crack_extends_to_boundary =
    this->MaterialMesh.is_boundary(find_opposite_edge(this->MaterialMesh, new_vertex.Face, new_vertex.OriginVertex));


  FaceHandle far_face;
  VertexHandle far_opposite_vertex;
  Matrix2f far_stress_tensor;

  // There is a far face to consider only if the near face is not a boundary face.
  if (crack_extends_to_boundary == false)
    {
    far_face =
      face_across_opposite_edge(this->MaterialMesh, near_face, new_vertex.OriginVertex);
    assert(far_face.is_valid());
    far_opposite_vertex =
      vertex_opposite_edge(this->MaterialMesh, far_face, new_vertex.Edge);
    far_stress_tensor = this->MaterialMesh.property(this->StressTensor, far_face);
    this->add_residual_separation_tensor(new_vertex.OriginVertex,
                                         far_opposite_vertex,
                                         new_vertex.CrackPlaneNormal);
    }


//  HalfedgeHandle he = this->MaterialMesh.halfedge_handle(new_vertex.Edge, 0);

  // This is where the faces actually get split
  this->MaterialMesh.split_copy(new_vertex.Edge, new_crack_tip);
  if (this->Verbosity > 4)
    {
    std::cout << "Faces after splitting edge:\n";
    this->debug_print_vertices();
    this->debug_print_faces();
    }

  // Restore the stress tensors for the faces we just created
  this->fix_faces_after_split(new_crack_tip,
                              new_vertex.OriginVertex,
                              far_opposite_vertex,
                              near_stress_tensor,
                              far_stress_tensor,
                              (crack_extends_to_boundary == false));
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::enqueue_failing_vertices()
{
  for (vertex_iterator vertex_iter = this->MaterialMesh.vertices_sbegin();
       vertex_iter != this->MaterialMesh.vertices_end();
       ++vertex_iter)
    {
    if (this->MaterialMesh.property(this->VertexBelongsToIsolatedFace, *vertex_iter))
      {
      continue;
      }

    EigenPair3 separation_info =
      largest_eigenpair(this->separation_tensor(*vertex_iter));

    this->TotalSeparationStress += separation_info.first;
    
    this->MaterialMesh.property(this->LargestSeparationEigenvalue, *vertex_iter) = separation_info.first;

    if (separation_info.first > this->MaterialMesh.property(this->MaterialToughness, *vertex_iter))
      {
      // check neighborhood size to make sure this triangle can fail at all
      int n_faces = 0;
      FaceHandle last_face;
      for (MeshGeometry::ConstVertexFaceIter f_iter = this->MaterialMesh.cvf_iter(*vertex_iter);
           f_iter.is_valid();
           ++f_iter)
        {
        last_face = *f_iter;
        ++n_faces;
        }

      if (n_faces == 1)
        {
        std::cout << "INFO: Found isolated face "
                  << last_face << " adjacent to a vertex that should fail.  This will never do.\n";
        this->MaterialMesh.property(this->FaceIsIsolated, last_face) = true;
        this->MaterialMesh.property(this->StressTensor, last_face) = Matrix2f::Zero();
        for (MeshGeometry::ConstFaceVertexIter v_iter = this->MaterialMesh.cfv_iter(last_face);
             v_iter.is_valid();
             ++v_iter)
          {
          this->MaterialMesh.property(this->VertexBelongsToIsolatedFace, *v_iter) = true;
          }
        continue;
        }

      if (this->Verbosity > 3)
        {
        std::cout << "INFO: Vertex "
                  << *vertex_iter << " at ("
                  << this->vertex_simulation_position(*vertex_iter).transpose()
                  << ") should fail.  Separation eigenvalue is "
                  << separation_info.first
                  << ", material toughness is "
                  << this->MaterialMesh.property(this->MaterialToughness, *vertex_iter)
                  << " and crack plane normal is ("
                  << separation_info.second.transpose()
                  << ").\n";
        }
      this->enqueue_new_failure(*vertex_iter, separation_info);
      }

    }
}


// ----------------------------------------------------------------------

void
CrackTriangleMesh::enqueue_new_failure(VertexHandle const& vertex,
                                       EigenPair3 const& separation_info)
{
  this->FailingVertices.push(FailureRecord(vertex, separation_info.first, separation_info.second));
}

// ----------------------------------------------------------------------

// Given a vertex that needs to crack and a plane normal to the crack
// direction, find faces adjacent to the vertex where we might add
// crack geometry.

int
CrackTriangleMesh::find_crack_vertex_positions(VertexHandle const& center,
                                               Vector3f const& plane_normal,
                                               std::vector<CrackVertex>& results) const
{
  // Make sure we don't identify an existing vertex as a crack tip
  // twice.
  std::set<VertexHandle> vertices_already_used;
  std::vector<Vector3f> crack_locations_so_far;

  // XXX YOU ARE HERE - identify and zero out isolated triangles

  int neighborhood_size = 0;
#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "DEBUG: find_crack_vertex_positions: Faces surrounding vertex "
            << center << ":";
#endif
  FaceHandle last_face;
  for (MeshGeometry::ConstVertexFaceIter face_iter = this->MaterialMesh.cvf_begin(center);
       face_iter.is_valid();
       ++face_iter)
    {
    ++neighborhood_size;
    last_face = *face_iter;
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << " " << *face_iter;
#endif
    }
#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "\n";
#endif

  if (neighborhood_size == 1)
    {
    std::cout << "Found disconnected triangle "
              << last_face << ".\n";
    return 0;
    }
  

  for (MeshGeometry::ConstVertexFaceIter face_iter = this->MaterialMesh.cvf_begin(center);
       face_iter.is_valid();
       ++face_iter)
    {
    HalfedgeHandle opposite_halfedge = find_opposite_halfedge(this->MaterialMesh, *face_iter, center);
    VertexHandle to_vertex(this->MaterialMesh.to_vertex_handle(opposite_halfedge));
    VertexHandle from_vertex(this->MaterialMesh.from_vertex_handle(opposite_halfedge));

    // OK, we've got it.  Where in that line segment are we?
    Vector3f edge_begin(this->vertex_simulation_position(from_vertex));
    Vector3f edge_end(this->vertex_simulation_position(to_vertex));
    Vector3f edge_direction(edge_end - edge_begin);
    bool solution_exists = false;

    double d = intersect_plane_with_line(this->vertex_simulation_position(center),
                                         plane_normal,
                                         edge_begin,
                                         edge_direction,
                                         &solution_exists);

    VertexHandle endpoint_vertex;

    if (solution_exists == false)
      {
      // Not on this line at all
      continue;
      }
    else if (fabs(d) > 1e-5 && fabs(1-d) > 1e-5 && (d < 0 || d > 1))
      {
      // The crack plane does not intersect this edge
      continue;
      }
    else
      {
      // The crack plane intersects this line somewhere.
      if (d > 0 && d < 1e-5)
        {
        // Right at the beginning of the line segment
        if (vertices_already_used.find(from_vertex) == vertices_already_used.end())
          {
          // OK, we can use this one once
          vertices_already_used.insert(from_vertex);
          endpoint_vertex = from_vertex;
          }
        else
          {
          continue; // skip this, we've already used this vertex
          }
        }
      else if (d < 1 && (1-d) < 1e-5)
        {
        // we're right at the end of the line segment
        if (vertices_already_used.find(to_vertex) == vertices_already_used.end())
          {
          // OK, we can use this one once
          vertices_already_used.insert(to_vertex);
          endpoint_vertex = to_vertex;
          }
        else
          {
          continue; // skip this, we've already used this vertex
          }
        }

      Vector3f new_vertex;
      if (endpoint_vertex.is_valid())
        {
        // The crack plane is basically one of the edges of the face.
        new_vertex = this->vertex_simulation_position(endpoint_vertex);
        }
      else
        {
        // The crack plane is in the interior of the face.
        new_vertex = edge_begin + d * edge_direction;
        }

      // Make sure we're not already trying to put a crack in this
      // location.
      bool crack_record_is_new = true;

      for (std::vector<Vector3f>::const_iterator location_iter = crack_locations_so_far.begin();
           location_iter != crack_locations_so_far.end();
           ++location_iter)
        {
        if ((new_vertex - *location_iter).norm() < 0.01)
          {
          if (this->Verbosity > 3)
            {
            std::cout << "We're already creating a crack tip at (" << new_vertex.transpose() << ").\n";
            crack_record_is_new = false;
            break;
            }
          }
        }

      if (crack_record_is_new)
        {
        if (this->Verbosity > 3)
          {
          std::cout << "\tCreating new crack record for center vertex "
                    << center << " at position ("
                    << this->vertex_simulation_position(center).transpose()
                    << ") with tip at (" << new_vertex.transpose() << ")\n";
          }

        crack_locations_so_far.push_back(new_vertex);

        CrackVertex crack_record(*face_iter, this->MaterialMesh.edge_handle(opposite_halfedge), center, new_vertex, plane_normal);
        if (endpoint_vertex.is_valid())
          {
          crack_record.VertexIsNew = false;
          }
        else
          {
          crack_record.VertexIsNew = true;
          }
        results.push_back(crack_record);
        }
      }
    }

  // Sanity check
  for (std::vector<CrackVertex>::iterator iter = results.begin();
       iter != results.end();
       ++iter)
    {
    assert(iter->Face.is_valid());
    }

  if (this->Verbosity > 2)
    {
    std::cout << "DEBUG: Found " << results.size() << " intersections between crack plane and existing geometry.\n";
    }

  return results.size();
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::find_perforation_corners(std::vector<HalfedgePair>& results) const
{
  std::vector<FaceHandle> faces_with_perf;
  results.clear();

  for (MeshGeometry::FaceIter f_iter = this->MaterialMesh.faces_sbegin();
       f_iter != this->MaterialMesh.faces_end();
       ++f_iter)
    {
    int boundary_count = count_boundary_edges(*f_iter, this->MaterialMesh);
    if (boundary_count == 2)
      {
      std::cout << "Face " << *f_iter << " has a perforation corner.\n";
      results.push_back(find_perforation_corner_in_face(*f_iter, this->MaterialMesh));
      }
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::finish_computing_separation_tensor(VertexHandle const& vertex)
{
  if (this->MaterialMesh.property(this->VertexIsFixed, vertex))
    return; // no separation force on fixed vertices

  Matrix3f& separation_tensor(
    this->MaterialMesh.property(this->SeparationTensor, vertex)
    );

  Vector3f& tensile_force(
    this->MaterialMesh.property(this->TotalTensileForce, vertex)
    );

  Vector3f& compressive_force(
    this->MaterialMesh.property(this->TotalCompressiveForce, vertex)
    );

  ensure_no_nans(separation_tensor);
  ensure_no_nans(tensile_force);
  ensure_no_nans(compressive_force);

  Matrix3f tensile_addition(
    construct_matrix_with_desired_eigenvector(tensile_force)
    );

  ensure_no_nans(tensile_addition);

  Matrix3f compressive_addition(
    construct_matrix_with_desired_eigenvector(compressive_force)
    );

  ensure_no_nans(compressive_addition);

  separation_tensor -= tensile_addition;
  separation_tensor += compressive_addition;

  separation_tensor *= 0.5;

  ensure_no_nans(separation_tensor);

  if (separation_tensor != Matrix3f::Zero() && this->Verbosity > 5)
    {
    std::cout << "Total separation tensor at vertex ("
              << this->vertex_simulation_position(vertex).transpose() << "):\n" << separation_tensor << "\n";
    }

}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::fix_boundary_edges()
{
  // We can use OpenMesh machinery to iterate over the boundary edges.
  // Here at the beginning there will only be one boundary... as long
  // as we're dealing with a square mesh!

  MeshGeometry::HalfedgeHandle he_handle, he_start_handle;
  int boundary_flags_set = 0;
  for (MeshGeometry::HalfedgeIter he_iter = this->MaterialMesh.halfedges_begin();
       he_iter != this->MaterialMesh.halfedges_end();
       ++he_iter)
    {
    if (this->MaterialMesh.is_boundary(*he_iter))
      {
      he_start_handle = *he_iter;
      he_handle = this->MaterialMesh.next_halfedge_handle(he_start_handle);

      if (this->Verbosity > 10)
        {
        std::cout << "DEBUG: Nailing down edge between ("
                  << this->vertex_simulation_position(this->MaterialMesh.from_vertex_handle(he_handle)).transpose()
                  << ") and ("
                  << this->vertex_simulation_position(this->MaterialMesh.to_vertex_handle(he_handle)).transpose()
                  << ")\n";
        }

      this->set_edge_fixed(this->MaterialMesh.edge_handle(he_handle), true);
      ++boundary_flags_set;

      while (he_handle != he_start_handle)
        {

        if (this->Verbosity > 10)
          {
          std::cout << "DEBUG: Nailing down edge between ("
                    << this->vertex_simulation_position(this->MaterialMesh.from_vertex_handle(he_handle)).transpose()
                    << ") and ("
                    << this->vertex_simulation_position(this->MaterialMesh.to_vertex_handle(he_handle)).transpose()
                    << ")\n";
          }

        this->MaterialMesh.property(this->VertexIsFixed,
                                    this->MaterialMesh.from_vertex_handle(he_handle)) = true;

        this->MaterialMesh.property(this->VertexIsFixed,
                                    this->MaterialMesh.to_vertex_handle(he_handle)) = true;

        this->set_edge_fixed(this->MaterialMesh.edge_handle(he_handle), true);
        he_handle = this->MaterialMesh.next_halfedge_handle(he_handle);
        ++boundary_flags_set;
        }
      if (this->Verbosity > 5)
        {
        std::cout << "DEBUG: fix_boundary_edges: Set "
                  << boundary_flags_set
                  << " edges to fixed\n";
        }
      return;
      }
    }

  if (this->Verbosity > 1)
    {
    std::cout << "DEBUG: No boundary edges found\n";
    }
}


// ----------------------------------------------------------------------

void
CrackTriangleMesh::fix_faces_after_split(
  VertexHandle const& crack_center_vertex,
  VertexHandle const& near_face_vertex,
  VertexHandle const& far_face_vertex,
  Matrix2f const& near_stress_tensor,
  Matrix2f const& far_stress_tensor,
  bool far_face_exists
  )
{
  for (MeshGeometry::VertexOHalfedgeIter he_iter = this->MaterialMesh.voh_iter(crack_center_vertex);
       he_iter.is_valid();
       ++he_iter)
    {
    if (this->MaterialMesh.to_vertex_handle(*he_iter) == near_face_vertex)
      {
      this->initialize_face_fragment(this->MaterialMesh.face_handle(*he_iter), near_stress_tensor);
      this->initialize_face_fragment(this->MaterialMesh.opposite_face_handle(*he_iter), near_stress_tensor);
      }
    else if (far_face_exists && (this->MaterialMesh.to_vertex_handle(*he_iter) == far_face_vertex))
      {
      this->initialize_face_fragment(this->MaterialMesh.face_handle(*he_iter), far_stress_tensor);
      this->initialize_face_fragment(this->MaterialMesh.opposite_face_handle(*he_iter), far_stress_tensor);
      }
    else
      {
      EdgeHandle edge(this->MaterialMesh.edge_handle(*he_iter));
      this->MaterialMesh.property(this->EdgeIsFixed, edge) = false;
      }
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::get_face_vertex_positions(FaceHandle const& face,
                                             std::vector<Vector3f>& output) const
{
  output.reserve(3);

  for (MeshGeometry::ConstFaceVertexIter vertex_iter = this->MaterialMesh.cfv_iter(face);
       vertex_iter.is_valid();
       ++vertex_iter)
    {
    output.push_back(this->vertex_simulation_position(*vertex_iter));
    }
}

// ----------------------------------------------------------------------

bool
CrackTriangleMesh::topology_has_changed() const
{
  return this->TopologyHasChanged;
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::identify_disconnected_faces()
{
  int num_isolated_faces = 0;

  for (MeshGeometry::ConstFaceIter f_iter = this->MaterialMesh.faces_sbegin();
       f_iter != this->MaterialMesh.faces_end();
       ++f_iter)
    {
    if (this->MaterialMesh.property(this->FaceIsIsolated, *f_iter))
      {
      continue;
      }

    bool this_face_is_isolated = true;

    for (MeshGeometry::ConstFaceHalfedgeIter h_iter = this->MaterialMesh.cfh_iter(*f_iter);
         h_iter.is_valid();
         ++h_iter)
      {
      HalfedgeHandle opposite(this->MaterialMesh.opposite_halfedge_handle(*h_iter));
      if (!this->MaterialMesh.is_boundary(opposite))
        {
        this_face_is_isolated = false;
        break;
        }
      }

    if (this_face_is_isolated)
      {
      this->MaterialMesh.property(this->FaceIsIsolated, *f_iter) = true;
      this->MaterialMesh.property(this->StressTensor, *f_iter) = Matrix2f::Zero();
      }
    }

  if (num_isolated_faces > 0)
    {
    std::cout << "identify_disconnected_faces: Found "
              << num_isolated_faces << " disconnected triangles\n";
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::initialize_edge_property_values(EdgeHandle const& e)
{
  this->MaterialMesh.property(this->EdgeIsFixed, e) = false;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::initialize_face_fragment(FaceHandle const& face,
                                            Matrix2f const& stress_tensor)
{
  assert(this->MaterialMesh.valence(face) == 3);
  this->initialize_face_property_values(face);
  this->MaterialMesh.property(this->StressTensor, face) = stress_tensor;
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::initialize_face_property_values(FaceHandle const& f)
{
  assert(this->MaterialMesh.valence(f) == 3);
  this->MaterialMesh.property(this->StressTensor, f) = Matrix2f::Zero();
  this->MaterialMesh.property(this->FaceIsIsolated, f) = false;

  this->compute_face_area(f);
  this->compute_barycentric_basis(f);
  assert(this->MaterialMesh.valence(f) == 3);
  assert(this->MaterialMesh.valence(f) == 3);

  for (int i = 0; i < 3; ++i)
    {
    for (int j = 0; j < 3; ++j)
      {
      assert(this->MaterialMesh.valence(f) == 3);
      this->MaterialMesh.property(this->StressTensorDerivatives[i][j], f) = Matrix2f::Zero();
      }
    }

  this->compute_stress_tensor_derivatives(f);
  assert(this->MaterialMesh.valence(f) == 3);

}
//----------------------------------------------------------------------

void CrackTriangleMesh::initialize_mesh_properties()
{
  // Vertex properties
  this->MaterialMesh.add_property(this->LargestSeparationEigenvalue);
  this->MaterialMesh.add_property(this->LatestVertexDisplacement);
  this->MaterialMesh.add_property(this->MaterialToughness);
  this->MaterialMesh.add_property(this->MaterialVertexSimulationPosition);
  this->MaterialMesh.add_property(this->SeparationTensor);
  this->MaterialMesh.add_property(this->SeparationTensorResidual);
  this->MaterialMesh.add_property(this->TotalCompressiveForce);
  this->MaterialMesh.add_property(this->TotalTensileForce);
  this->MaterialMesh.add_property(this->VertexBelongsToIsolatedFace);
  this->MaterialMesh.add_property(this->VertexIsOnCrackBoundary);
  this->MaterialMesh.add_property(this->VertexIsCrackTip);
  this->MaterialMesh.add_property(this->VertexIsFixed);

  // Face properties
  this->MaterialMesh.add_property(this->BarycentricBasisMatrix);
  this->MaterialMesh.add_property(this->FaceArea);
  this->MaterialMesh.add_property(this->FaceIsIsolated);
  this->MaterialMesh.add_property(this->StressTensor);
  for (int i = 0; i < 3; ++i)
    {
    for (int j = 0; j < 3; ++j)
      {
      this->MaterialMesh.add_property(this->StressTensorDerivatives[i][j]);
      }
    }

  // Edge properties
  this->MaterialMesh.add_property(this->EdgeIsFixed);

  // Add the ability to delete elements
  this->MaterialMesh.request_face_status();
  this->MaterialMesh.request_edge_status();
  this->MaterialMesh.request_vertex_status();

  this->initialize_mesh_property_values();

}

// ----------------------------------------------------------------------

void CrackTriangleMesh::initialize_mesh_property_values()
{
  int num_faces = 0, num_vertices = 0, num_edges = 0;

  for (vertex_iterator vit = this->MaterialMesh.vertices_sbegin();
       vit != this->MaterialMesh.vertices_end();
       ++vit, ++num_vertices)
    {
    this->initialize_vertex_property_values(*vit);
    }

  for (face_iterator fit = this->MaterialMesh.faces_sbegin();
       fit != this->MaterialMesh.faces_end();
       ++fit, ++num_faces)
    {
    this->initialize_face_property_values(*fit);
    }

  for (edge_iterator eit = this->MaterialMesh.edges_sbegin();
       eit != this->MaterialMesh.edges_end();
       ++eit, ++num_edges)
    {
    this->initialize_edge_property_values(*eit);
    }

  if (this->Verbosity > 2)
    {
    std::cout << "DEBUG: Initialized properties for "
              << num_vertices << " vertices, "
              << num_faces << " faces and "
              << num_edges << " edges.\n";
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::initialize_neighboring_faces(VertexHandle const& vertex)
{
  for (MeshGeometry::ConstVertexFaceIter f_iter = this->MaterialMesh.cvf_iter(vertex);
       f_iter.is_valid();
       ++f_iter)
    {
    if (this->MaterialMesh.valence(*f_iter) == 3)
      {
      this->initialize_face_property_values(*f_iter);
      }
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::initialize_vertex_property_values(VertexHandle const& v)
{
  this->MaterialMesh.property(this->LargestSeparationEigenvalue, v) = 0;
  this->MaterialMesh.property(this->LatestVertexDisplacement, v) = Vector3f::Zero();
  this->MaterialMesh.property(this->MaterialToughness, v) = this->Toughness;
  this->MaterialMesh.property(this->SeparationTensor, v) = Matrix3f::Zero();
  this->MaterialMesh.property(this->SeparationTensorResidual, v) = Matrix3f::Zero();
  this->MaterialMesh.property(this->TotalCompressiveForce, v) = Vector3f::Zero();
  this->MaterialMesh.property(this->TotalTensileForce, v) = Vector3f::Zero();
  this->MaterialMesh.property(this->VertexBelongsToIsolatedFace, v) = false;
  this->MaterialMesh.property(this->VertexIsCrackTip, v) = false;
  this->MaterialMesh.property(this->VertexIsFixed, v) = false;
  this->MaterialMesh.property(this->MaterialVertexSimulationPosition, v) =
    ::vector3_om_to_eigen(this->MaterialMesh.point(v));
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::initialize_stress_field(DynamicArray const& stress_image)
{
  if (this->MaterialMesh.n_faces() == 0)
    {
    std::cout << "ERROR: You must initialize the simulation mesh before you can initialize the stress field.\n";
    return;
    }

  int image_width = stress_image_width(stress_image);
  int image_height = stress_image_height(stress_image);
  
  std::pair<MeshGeometry::Point, MeshGeometry::Point> mesh_bounds = compute_mesh_bounding_box(this->MaterialMesh);

  MeshGeometry::Point span = mesh_bounds.second - mesh_bounds.first;

  for (MeshGeometry::ConstFaceIter f_iter = this->MaterialMesh.faces_sbegin();
       f_iter != this->MaterialMesh.faces_end();
       ++f_iter)
    {
    MeshGeometry::Point face_center(0, 0, 0);
    for (MeshGeometry::ConstFaceVertexIter v_iter = this->MaterialMesh.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      face_center += this->MaterialMesh.point(*v_iter);
      }
    // divide by 3 vertices to get the average vertex position
    face_center /= 3;

    MeshGeometry::Point normalized_face_center(face_center);

    // Scale and translate the coordinates of the face center to match
    // the dimensions of the input image
    normalized_face_center -= mesh_bounds.first;

    normalized_face_center[0] /= span[0];
    normalized_face_center[1] /= span[1];

    normalized_face_center[0] *= image_width;
    normalized_face_center[1] *= image_height;

    int pixel_x = static_cast<int>(normalized_face_center[0]);
    int pixel_y = static_cast<int>(normalized_face_center[1]);

    if (pixel_x < 0) pixel_x = 0;
    if (pixel_x >= image_width) pixel_x = image_width - 1;
    if (pixel_y < 0) pixel_y = 0;
    if (pixel_y >= image_height) pixel_y = image_height - 1;

    this->MaterialMesh.property(this->StressTensor, *f_iter) =
      Matrix2f::Identity() * stress_image(pixel_y, pixel_x);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::introduce_new_cracks()
{
  int failure_count = 0;
  
  while (this->FailingVertices.empty() == false)
    {
    FailureRecord next_failed_node(this->FailingVertices.top());
    this->FailingVertices.pop();
    ++failure_count;
    
    std::vector<CrackVertex> crack_vertices;
    int return_value = this->find_crack_vertex_positions(next_failed_node.Location,
                                                         next_failed_node.CrackPlaneNormal,
                                                         crack_vertices);

    this->sanity_check_faces();

    this->emplace_crack_vertices(crack_vertices);
    this->split_crack_center(next_failed_node.Location, crack_vertices);

    this->TopologyHasChanged = true;

    if (failure_count == 5)
      this->FailingVertices.clear(); // for the time being
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::mark_crack_tip(VertexHandle const& v, bool is_crack)
{
  this->MaterialMesh.property(this->VertexIsCrackTip, v) = is_crack;
  if (is_crack)
    {
    // Being a crack tip necessarily implies being on a crack border
    this->mark_vertex_on_crack_boundary(v, true);
    }
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::mark_vertex_on_crack_boundary(VertexHandle const& v, bool on_boundary)
{
  this->MaterialMesh.property(this->VertexIsOnCrackBoundary, v) = on_boundary;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::move_cosmetic_vertex(VertexHandle const& v,
                                        Vector3f const& how_far)
{
  MeshGeometry::Point current_location(this->MaterialMesh.point(v));
  current_location[0] += 0.025 * how_far(0);
  current_location[1] += 0.025 * how_far(1);
  current_location[2] += 0.025 * how_far(2);
  this->MaterialMesh.set_point(v, current_location);
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::populate_mesh_properties()
{
  this->transfer_vertices_to_simulation();
  this->compute_face_areas();
  this->compute_barycentric_basis();
  this->compute_stress_tensor_derivatives();
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::relax_stress_field()
{
  this->RelaxTimeStep = (this->CurrentTimeStep / this->NumRelaxStepsPerIteration);
  for (int i = 0; i < this->NumRelaxStepsPerIteration; ++i)
    {
    if (this->Verbosity > 8)
      {
      std::cout << "DEBUG: Taking " << (i+1) << " of " << this->NumRelaxStepsPerIteration << " tiny relaxation steps\n";
      }
    this->update_stress_tensors();
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::remove_crack_faces(std::vector<VertexHandle> const& crack_points)
{
  FaceHandle next_face = find_face_containing(this->MaterialMesh, crack_points);
  int num_faces_deleted = 0;
  while (next_face.is_valid())
    {
    this->MaterialMesh.delete_face(next_face, false);
    next_face = find_face_containing(this->MaterialMesh, crack_points);
//    this->add_face_to_crack_mesh(next_face);
    ++num_faces_deleted;
    }
  if (this->Verbosity > 2)
    {
    std::cout << "remove_crack_faces: Deleted " << num_faces_deleted << " crack faces.\n";
    }

  this->sanity_check_faces();
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::restore_stress_tensors_around_vertex(VertexHandle const& center_vertex,
                                                             std::map<EdgeHandle, Matrix2f> const& saved_tensors)
{
  for (MeshGeometry::ConstVertexFaceIter f_iter = this->MaterialMesh.cvf_iter(center_vertex);
       f_iter.is_valid();
       ++f_iter)
    {
    EdgeHandle opposite_edge(find_opposite_edge(this->MaterialMesh, *f_iter, center_vertex));

    std::map<EdgeHandle, Matrix2f>::const_iterator map_entry = saved_tensors.find(opposite_edge);
    if (map_entry != saved_tensors.end())
      {
      this->MaterialMesh.property(this->StressTensor, *f_iter) = (*map_entry).second;
      }
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::sanity_check_faces() const
{
  return;
#if !defined(NDEBUG)
  // Sanity check!
  VertexHandle vertices[3];

  for (MeshGeometry::ConstFaceIter f_iter = this->MaterialMesh.faces_sbegin();
       f_iter != this->MaterialMesh.faces_end();
       ++f_iter)
    {
    if (this->MaterialMesh.valence(*f_iter) != 3)
      {
      std::cout << "ERROR: sanity_check_faces: face "
                << *f_iter
                << " has "
                << this->MaterialMesh.valence(*f_iter)
                << " vertices!\n";
      assert(1==0);
      }

    int vertex_id = 0;
    for (MeshGeometry::ConstFaceVertexIter v_iter = this->MaterialMesh.cfv_iter(*f_iter);
         v_iter.is_valid();
         ++v_iter)
      {
      vertices[vertex_id++] = *v_iter;
      }
    if (vertices[0] == vertices[1] ||
        vertices[0] == vertices[2] ||
        vertices[1] == vertices[2])
      {
      std::cout << "** ERROR: DEGENERATE FACE " << *f_iter << "\n";
      assert(1==0);
      }
    }
#endif // only build if NDEBUG is not set
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::save_stress_tensors_around_vertex(VertexHandle const& v,
                                                          std::map<EdgeHandle, Matrix2f>& archive) const
{
  for (MeshGeometry::ConstVertexFaceIter f_iter = this->MaterialMesh.cvf_iter(v);
       f_iter.is_valid();
       ++f_iter)
    {
    EdgeHandle opposite_edge(find_opposite_edge(this->MaterialMesh, *f_iter, v));
    assert(opposite_edge.is_valid());

    archive[opposite_edge] = this->stress_tensor(*f_iter);
    }
}

// ----------------------------------------------------------------------

Matrix3f CrackTriangleMesh::separation_tensor(VertexHandle const& vertex) const
{
  return (this->MaterialMesh.property(this->SeparationTensor, vertex) +
          this->MaterialMesh.property(this->SeparationTensorResidual, vertex));
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::set_edge_fixed(EdgeHandle const& eh,
                                       bool is_fixed)
{

  this->MaterialMesh.property(this->EdgeIsFixed, eh) = is_fixed;

}

//----------------------------------------------------------------------

void CrackTriangleMesh::set_initial_geometry(MeshGeometry const& g)
{
  this->CurrentIteration = 0;
  this->MaterialMesh = g;
  this->initialize_mesh_properties();
  this->populate_mesh_properties();
  this->TopologyHasChanged = true;
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::set_material_toughness(float how_tough)
{
  this->Toughness = how_tough;

  for (vertex_iterator vertex_iter = this->MaterialMesh.vertices_sbegin();
       vertex_iter != this->MaterialMesh.vertices_end();
       ++vertex_iter)
    {
    this->MaterialMesh.property(this->MaterialToughness, *vertex_iter) = how_tough;
    }
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::set_verbosity(int how_verbose)
{
  this->Verbosity = how_verbose;
}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::snap_crack_to_nearby_edges(CrackVertex& new_crack) const
{
  HalfedgeHandle opposite_edge = find_opposite_halfedge(this->MaterialMesh, new_crack.Face, new_crack.OriginVertex);
  VertexHandle from_handle = this->MaterialMesh.from_vertex_handle(opposite_edge);
  VertexHandle to_handle = this->MaterialMesh.to_vertex_handle(opposite_edge);

  Vector3f v_crack(new_crack.CrackVertexPosition);
  Vector3f v_origin(this->vertex_simulation_position(new_crack.OriginVertex));
  Vector3f v_from(this->vertex_simulation_position(from_handle));
  Vector3f v_to(this->vertex_simulation_position(to_handle));

#if defined(COPIOUS_DEBUG_OUTPUT)

  std::cout << "Snap: Crack center position is ("
            << v_origin.transpose() << "), vertex "
            << new_crack.OriginVertex << "\n";
  std::cout << "Snap: Proposed crack vertex is ("
            << v_crack.transpose() << ")\n";
  std::cout << "Snap: From vertex is ("
            << v_from.transpose() << "), To vertex is ("
            << v_to.transpose() << ")\n";
#endif

  float angle_with_v_to = 0;
  float angle_with_v_from = 0;
  bool to_vertex_on_crack = false;
  bool from_vertex_on_crack = false;

  angle_with_v_to = angle_between(v_to - v_origin, v_crack - v_origin);
  angle_with_v_from = angle_between(v_from - v_origin, v_crack - v_origin);

  // Now we have to decide exactly which vertex to snap to, if any.
  float from_angle_threshold, to_angle_threshold;
  if (this->vertex_is_on_crack_boundary(from_handle))
    {
    from_vertex_on_crack = true;
    from_angle_threshold = this->CrackEdgeSnapThreshold;
    }
  else
    {
    from_angle_threshold = this->InteriorEdgeSnapThreshold;
    }
  if (this->vertex_is_on_crack_boundary(to_handle))
    {
    to_vertex_on_crack = true;
    to_angle_threshold = this->CrackEdgeSnapThreshold;
    }
  else
    {
    to_angle_threshold = this->CrackEdgeSnapThreshold;
    }

  // Test #1: Do we need to snap at all?
  if (angle_with_v_to > to_angle_threshold &&
      angle_with_v_from > from_angle_threshold)
    {
    // No, we don't.
    return;
    }
  else if ((angle_with_v_to > to_angle_threshold) !=
           (angle_with_v_from > from_angle_threshold))
    {
    // Yes, we do, and there's only one possibility.
    VertexHandle target_vertex =
      (angle_with_v_to <= to_angle_threshold) ? to_handle : from_handle;

    Vector3f old_position = new_crack.CrackVertexPosition;
    new_crack.CrackVertexPosition = this->vertex_simulation_position(target_vertex);
    new_crack.CrackVertexHandle = target_vertex;
    new_crack.VertexIsNew = false;
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "Snapping crack vertex ("
              << old_position.transpose()
              << ") to existing vertex ("
              << new_crack.CrackVertexPosition.transpose()
              << ").\n";
#endif
    }
  else
    {
    // Yes, we do have to snap to an existing vertex, but both are
    // candidates.  If one is a crack vertex and one is not, prefer
    // that one.  Otherwise just pick whichever one is closer.
    VertexHandle target_vertex;
    if (to_vertex_on_crack != from_vertex_on_crack)
      {
      target_vertex = (to_vertex_on_crack ? to_handle : from_handle);
      }
    else
      {
      target_vertex = (angle_with_v_to < angle_with_v_from) ? to_handle : from_handle;
      }

    Vector3f old_position = new_crack.CrackVertexPosition;
    new_crack.CrackVertexPosition = this->vertex_simulation_position(target_vertex);
    new_crack.CrackVertexHandle = target_vertex;
    new_crack.VertexIsNew = false;

#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "Snapping crack vertex ("
              << old_position.transpose()
              << ") to existing vertex ("
              << new_crack.CrackVertexPosition.transpose()
              << ").\n";
#endif
    }
  // Done snapping proposed crack edge to existing edge
}


// ----------------------------------------------------------------------

// The whole point of cracking the mesh is that we introduce a gap
// where there used to be a vertex.  We have to take a single vertex
// and turn it into two vertices connected by an edge, then remove any
// faces that got created by that split operation.  It can get tricky
// in cases where we're meeting an existing crack or intersecting a
// material boundary.
//
// The most robust way to do this is to walk the edges around the
// crack vertex and look for ranges that begin and end with boundary
// edges.  Each of those ranges is going to get its own copy of the
// central vertex.  One of them (arbitrarily picking the first one)
// can keep the original central vertex.

bool CrackTriangleMesh::halfedge_is_crack_or_opposite_boundary(
  HalfedgeHandle const& he,
  std::set<VertexHandle> const& crack_tips
  ) const
{
  VertexHandle v_from = this->MaterialMesh.from_vertex_handle(he);
  VertexHandle v_to = this->MaterialMesh.to_vertex_handle(he);
  HalfedgeHandle opposite_he = this->MaterialMesh.opposite_halfedge_handle(he);

  if (crack_tips.find(v_from) != crack_tips.end() ||
      crack_tips.find(v_to) != crack_tips.end() ||
      this->MaterialMesh.is_boundary(opposite_he))
    {
    return true;
    }
  else
    {
    return false;
    }
}

// ----------------------------------------------------------------------

bool CrackTriangleMesh::halfedge_is_crack_or_boundary(
  HalfedgeHandle const& he,
  std::set<VertexHandle> const& crack_tips
  ) const
{
  VertexHandle v_from = this->MaterialMesh.from_vertex_handle(he);
  VertexHandle v_to = this->MaterialMesh.to_vertex_handle(he);

  if (crack_tips.find(v_from) != crack_tips.end() ||
      crack_tips.find(v_to) != crack_tips.end() ||
      this->MaterialMesh.is_boundary(he))
    {
    return true;
    }
  else
    {
    return false;
    }
}

// ----------------------------------------------------------------------

std::size_t CrackTriangleMesh::find_first_boundary_halfedge(
  VertexHandle const& center_vertex,
  std::set<VertexHandle> const& crack_tips
  ) const
{
  MeshGeometry::ConstVertexOHalfedgeCCWIter he_iter = this->MaterialMesh.cvoh_ccwiter(center_vertex);
  std::size_t here_index = 0;

  // Walk CCW around the outgoing half-edges until we find one whose opposite
  // is a boundary edge or that connects the crack center to one of
  // the crack tips.
  while ((this->halfedge_is_crack_or_opposite_boundary(*he_iter, crack_tips) == false)
         && he_iter->is_valid())
    {
    ++he_iter;
    ++here_index;
    }

  if (!he_iter.is_valid())
    {
    std::cout << "WARNING: Couldn't find a boundary or crack edge around vertex "
              << center_vertex << ".\n";
    return 99999999;
    }
  else
    {
    return here_index;
    }
}

// ----------------------------------------------------------------------

HalfedgeHandle CrackTriangleMesh::advance_halfedge_past_gap(HalfedgeHandle const& start) const
{
  assert(this->MaterialMesh.is_boundary(start));
  HalfedgeHandle prev = this->MaterialMesh.prev_halfedge_handle(start);
  HalfedgeHandle next = this->MaterialMesh.next_halfedge_handle(start);
  HalfedgeHandle flip = this->MaterialMesh.opposite_halfedge_handle(prev);

  std::cout << "advance_halfedge (1): Next edge after " << start << " is "
            << ::halfedge_as_string(next, this->MaterialMesh) << "\n";

  std::cout << "advance_halfedge (2): Previous edge before " << start << " is "
            << ::halfedge_as_string(prev, this->MaterialMesh) << "\n";

  std::cout << "advance_halfedge (3): Opposite of previous edge is "
            << ::halfedge_as_string(flip, this->MaterialMesh) << "\n";

  return flip;
}

// ----------------------------------------------------------------------

HalfedgeHandle CrackTriangleMesh::advance_halfedge_to_next_face(HalfedgeHandle const& start) const
{
  HalfedgeHandle next_this_face = this->MaterialMesh.next_halfedge_handle(start);
  next_this_face = this->MaterialMesh.next_halfedge_handle(next_this_face);
  return this->MaterialMesh.opposite_halfedge_handle(next_this_face);
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::split_crack_center(VertexHandle const& center_vertex,
                                           std::vector<CrackVertex> const& new_crack_vertices)
{
#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "DEBUG: Before split_crack_center, mesh is:\n";
  this->debug_print_mesh_topology();
#endif

  this->MaterialMesh.property(this->SeparationTensorResidual, center_vertex) = Matrix3f::Zero();
  
  std::set<VertexHandle> crack_tips;
  for (std::vector<CrackVertex>::const_iterator iter = new_crack_vertices.begin();
       iter != new_crack_vertices.end();
       ++iter)
    {
    crack_tips.insert(iter->CrackVertexHandle);
    }

  std::vector<HalfedgeHandle> vertex_neighborhood;

#define SUCCESSOR(i) (vertex_neighborhood[(i+1) % vertex_neighborhood.size()])
#define WRAPAROUND(i) ((i) % vertex_neighborhood.size())

#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "Half-edges surrounding center vertex:\n";
#endif

  for (MeshGeometry::ConstVertexOHalfedgeCCWIter iter = this->MaterialMesh.cvoh_ccwiter(center_vertex);
       iter.is_valid();
       ++iter)
    {
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "  ";
    this->debug_print_halfedge(*iter);
#endif
    vertex_neighborhood.push_back(*iter);
    }

#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "Vertex neighborhood has "
            << vertex_neighborhood.size()
            << " elements\n";

  if (vertex_neighborhood.size() == 2)
    {
    int num_faces = 0;
    for (MeshGeometry::ConstVertexFaceIter f_iter = this->MaterialMesh.cvf_begin(center_vertex);
         f_iter.is_valid();
         ++f_iter)
      {
      ++num_faces;
      }
    std::cout << "Vertex adjoins "
              << num_faces << " faces\n";
    }
#endif
  
#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "DEBUG: Building vertex neighborhood\n";
  for (std::size_t i = 0; i < vertex_neighborhood.size(); ++i)
    {
    HalfedgeHandle here = vertex_neighborhood[i];
    HalfedgeHandle next = SUCCESSOR(i);
    std::cout << "Successor of halfedge "
              << ::halfedge_as_string(here, this->MaterialMesh)
              << " is "
              << ::halfedge_as_string(next, this->MaterialMesh)
              << "\n";
    }
#endif

  std::size_t first_boundary_halfedge_index = this->find_first_boundary_halfedge(center_vertex, crack_tips);

  HalfedgeHandle original_starting_halfedge = vertex_neighborhood[first_boundary_halfedge_index];

#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "DEBUG: Original boundary-ish halfedge is "
            << ::halfedge_as_string(original_starting_halfedge, this->MaterialMesh) << "\n";
#endif

  // Now we increment that so that we're one past the current
  // boundary, then walk over edges until we find the next boundary
  int num_steps = 0;
  std::size_t he_current_index = WRAPAROUND(first_boundary_halfedge_index + 1);
  HalfedgeHandle he_face_start(vertex_neighborhood[he_current_index]);

  // No!  This assertion is false when we're extending an existing crack.
  // assert(this->MaterialMesh.face_handle(original_starting_halfedge).is_valid());

  // Skip past faces until we reach another boundary or crack edge.
  while (this->halfedge_is_crack_or_boundary(he_face_start, crack_tips) == false)
    {
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "Skipping past non-boundary/non-crack halfedge: ";
    this->debug_print_halfedge(he_face_start);
#endif
    assert(he_face_start.is_valid());
    assert(he_face_start != original_starting_halfedge);
    he_current_index = WRAPAROUND(he_current_index + 1);
    he_face_start = vertex_neighborhood[he_current_index];
    ++num_steps;
    }

#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "DEBUG: Done skipping past first face range.  num_steps is "
            << num_steps << "\n";
  std::cout << "DEBUG: Starting halfedge for first replace-faces range is "
            << ::halfedge_as_string(he_face_start, this->MaterialMesh)
            << "\n";
#endif

  // We've just passed the range that we leave alone.  From here on
  // out, we will be replacing faces with duplicates that use a
  // different center vertex.

  int num_face_ranges = 1;
  int num_gaps = 0;

  std::vector<FaceAction> mesh_operations;

  // Now we need to copy faces until we hit another boundary edge.
  while (num_steps < vertex_neighborhood.size())
    {
    ++num_steps;

#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "Starting new face copy range at "
              << ::halfedge_as_string(he_face_start, this->MaterialMesh)
              << "\n";
 #endif
    ++num_face_ranges;

    // Skip forward until we find an edge that isn't a boundary.
    while (this->MaterialMesh.is_boundary(he_face_start))
      {
      ++num_gaps;
#if defined(COPIOUS_DEBUG_OUTPUT)
      std::cout << "Skipping past boundary halfedge: ";
      this->debug_print_halfedge(he_face_start);
#endif
      he_current_index = WRAPAROUND(he_current_index + 1);
      he_face_start = vertex_neighborhood[he_current_index];
      ++num_steps;
      }

    VertexHandle new_center_vertex = this->duplicate_vertex(center_vertex);

    assert(he_face_start.is_valid());
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "DEBUG: After skipping gaps, face-start halfedge is ";
    this->debug_print_halfedge(he_face_start);
    std::cout << "New copy of center vertex is "
              << new_center_vertex << "\n";
    std::cout << "DEBUG: Starting copy loop with he_face_start as ";
    this->debug_print_halfedge(he_face_start);
    std::cout << "So far we have moved "
              << num_steps << " steps around the neighborhood\n";
#endif

    do
      {
      he_face_start = vertex_neighborhood[he_current_index];
      HalfedgeHandle he_face_end = vertex_neighborhood[WRAPAROUND(he_current_index + 1)];

#if defined(COPIOUS_DEBUG_OUTPUT)
      std::cout << "DEBUG: At top of copy loop, he_face_start is "
                << ::halfedge_as_string(he_face_start, this->MaterialMesh) << "\n";

      std::cout << "DEBUG: he_face_end is "
                << ::halfedge_as_string(he_face_end, this->MaterialMesh)
                << "\n";

      std::cout << "DEBUG: num_steps is "
                << num_steps << "\n";
#endif

      FaceHandle existing_face = this->MaterialMesh.face_handle(he_face_start);
      assert(existing_face.is_valid());

      VertexHandle v1 = this->MaterialMesh.to_vertex_handle(he_face_start);
      VertexHandle v2 = this->MaterialMesh.to_vertex_handle(he_face_end);

      FaceAction next_action;
      next_action.FaceToDelete = existing_face;
      next_action.FaceToAdd.push_back(new_center_vertex);
      next_action.FaceToAdd.push_back(v1);
      next_action.FaceToAdd.push_back(v2);
      next_action.SavedStressTensor = this->stress_tensor(existing_face);
#if defined(COPIOUS_DEBUG_OUTPUT)
      std::cout << "Enqueueing new face " << new_center_vertex
                << ", " << v1 << ", " << v2 << "\n";
#endif
      mesh_operations.push_back(next_action);

      he_current_index = WRAPAROUND(he_current_index + 1);
      he_face_start = he_face_end;
      ++num_steps;
      }
    while (this->halfedge_is_crack_or_boundary(he_face_start, crack_tips) == false &&
           num_steps < vertex_neighborhood.size());

  }

#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "** Performing remeshing operations around crack center\n";
#endif
  
  for (std::vector<::FaceAction>::const_iterator iter = mesh_operations.begin();
       iter != mesh_operations.end();
       ++iter)
    {
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "Deleting face " << iter->FaceToDelete << "\n";
#endif
    this->MaterialMesh.delete_face(iter->FaceToDelete);
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "Adding face with vertices "
              << iter->FaceToAdd[0] << ", "
              << iter->FaceToAdd[1] << ", "
              << iter->FaceToAdd[2] << "\n";
#endif
    FaceHandle new_face = this->MaterialMesh.add_face(iter->FaceToAdd);
#if defined(COPIOUS_DEBUG_OUTPUT)
    std::cout << "New face has ID " << new_face << "\n";
#endif
    this->initialize_face_property_values(new_face);
    this->MaterialMesh.property(this->StressTensor, new_face) = iter->SavedStressTensor;
    }

#if defined(COPIOUS_DEBUG_OUTPUT)
  std::cout << "DEBUG: split_crack_vertex: Processed "
            << num_face_ranges << " range(s) and "
            << num_gaps << " gap(s)\n";
#endif
  
#if 0
  this->disconnect_perforation_corners();
#endif
}

// ----------------------------------------------------------------------

Matrix2f const&
CrackTriangleMesh::stress_tensor(FaceHandle const& face) const
{
  return this->MaterialMesh.property(this->StressTensor, face);
}

// ----------------------------------------------------------------------

bool CrackTriangleMesh::stress_exceeds_failure_threshold(VertexHandle const& vertex) const
{
  // The failure threshold is stored in the material toughness
  // property.  The material will fail if the largest positive
  // eigenvalue of the separation tensor exceeds the toughness.

  return false;
}

// ----------------------------------------------------------------------

Vector3f CrackTriangleMesh::total_force_on_vertex(VertexHandle const& vertex) const
{
  return (
    this->MaterialMesh.property(this->TotalTensileForce, vertex) +
    this->MaterialMesh.property(this->TotalCompressiveForce, vertex)
    );
}

// ----------------------------------------------------------------------

void CrackTriangleMesh::transfer_vertices_to_simulation()
{
  for (vertex_iterator iter = this->MaterialMesh.vertices_sbegin();
       iter != this->MaterialMesh.vertices_end();
       ++iter)
    {
    this->MaterialMesh.property(this->MaterialVertexSimulationPosition, *iter) =
      ::vector3_om_to_eigen(this->MaterialMesh.point(*iter));
    }
}

// ----------------------------------------------------------------------

// This function implements equations 5-7 from Iben and O'Brien

void
CrackTriangleMesh::update_stress_tensor_at_face(FaceHandle const& face)
{
  assert(this->MaterialMesh.valence(face) == 3);

  // Derivative of the stress tensor with respect to X, Y and Z
  // components of the position of the current vertex
  int vertex_id = 0;
  bool something_changed = false;

  assert(face.is_valid());

  Matrix2f& current_stress_tensor(this->MaterialMesh.property(this->StressTensor, face));
  ensure_no_nans(current_stress_tensor);

  for (MeshGeometry::ConstFaceVertexIter vertex_iter =
         this->MaterialMesh.cfv_iter(face);
       vertex_iter.is_valid();
       ++vertex_iter, ++vertex_id)
    {
    for (int component = 0; component < 3; ++component)
      {
      if (vertex_id < 0 || vertex_id >= 3 || component < 0 || component >= 3)
        {
        std::cout << "ERROR: Something's wrong with this face.  Vertices are "
                  << this->face_coordinates_as_string(face) << "\n";
        assert(1==0);
        }
      assert(vertex_id >= 0 && vertex_id < 3);
      assert(component >= 0 && component < 3);

      Matrix2f const& derivative(
        this->MaterialMesh.property(
          this->StressTensorDerivatives[vertex_id][component],
          face
          )
        );

      Vector3f displacement(this->MaterialMesh.property(this->LatestVertexDisplacement, *vertex_iter));
      this->move_cosmetic_vertex(*vertex_iter, displacement);

      if (displacement != Vector3f::Zero())
        {
        something_changed = true;
        }

      ensure_no_nans(displacement);

      if (this->Verbosity > 6)
        {
        if (displacement != Vector3f::Zero())
          {
          std::cout << "Displacement of vertex: " << displacement.transpose() << "\n";
          std::cout << "Previous stress tensor:\n" << current_stress_tensor << "\n";
          }
        }

      current_stress_tensor += displacement[component] * derivative;
      ensure_no_nans(current_stress_tensor);

      } // done looping over components of this vertex
    } // done looping over vertices of this face

  if (this->Verbosity > 6 && something_changed)
    {
    std::cout << "New stress tensor:\n" << current_stress_tensor << "\n";
    }

}

// ----------------------------------------------------------------------

void
CrackTriangleMesh::update_stress_tensors()
{
  this->compute_all_vertex_displacements(this->RelaxTimeStep);

  for (face_iterator face_iter = this->MaterialMesh.faces_sbegin();
       face_iter != this->MaterialMesh.faces_end();
       ++face_iter)
    {
    this->update_stress_tensor_at_face(*face_iter);
    }
}

// ----------------------------------------------------------------------

int CrackTriangleMesh::verbosity() const
{
  return this->Verbosity;
}

// ----------------------------------------------------------------------

bool
CrackTriangleMesh::vertex_is_on_crack_boundary(VertexHandle const& v) const
{
  return this->MaterialMesh.property(this->VertexIsOnCrackBoundary, v);
}

// ----------------------------------------------------------------------

Vector3f
CrackTriangleMesh::vertex_simulation_position(VertexHandle const& v) const
{
  return this->MaterialMesh.property(this->MaterialVertexSimulationPosition, v);
}


// ----------------------------------------------------------------------

Vector3f
CrackTriangleMesh::crack_vertex_cosmetic_position(VertexHandle const& v) const
{
  return ::vector3_om_to_eigen(this->CrackMesh.point(v));
}

// ----------------------------------------------------------------------

Vector3f
CrackTriangleMesh::material_vertex_cosmetic_position(VertexHandle const& v) const
{
  return ::vector3_om_to_eigen(this->MaterialMesh.point(v));
}

// ----------------------------------------------------------------------

std::string
CrackTriangleMesh::face_coordinates_as_string(FaceHandle const& f) const
{
  std::ostringstream outbuf;
  for (MeshGeometry::ConstFaceVertexIter iter = this->MaterialMesh.cfv_iter(f);
       iter.is_valid();
       ++iter)
    {
    outbuf << "(" << this->vertex_simulation_position(*iter).transpose() << ") ";
    }
  return outbuf.str();
}

// ----------------------------------------------------------------------

MeshGeometry const&
CrackTriangleMesh::material_mesh() const
{
  return this->MaterialMesh;
}

// ----------------------------------------------------------------------

MeshGeometry const&
CrackTriangleMesh::crack_mesh() const
{
  return this->CrackMesh;
}
