#ifndef __GeometryFinders_h
#define __GeometryFinders_h

#include "CrackDataTypes.h"
#include <algorithm>

int count_boundary_edges(FaceHandle const& face, MeshGeometry const& mesh);

HalfedgePair find_perforation_corner_in_face(FaceHandle const& face, MeshGeometry const& mesh);

std::pair<MeshGeometry::Point, MeshGeometry::Point> compute_mesh_bounding_box(MeshGeometry const& mesh);

HalfedgeHandle find_opposite_halfedge(MeshGeometry const& mesh, FaceHandle const& face, VertexHandle const& vertex);

EdgeHandle find_opposite_edge(MeshGeometry const& mesh, FaceHandle const& face, VertexHandle const& vertex);

FaceHandle face_across_opposite_edge(MeshGeometry const& mesh,
                                     FaceHandle const& face1,
                                     VertexHandle const& vertex);

VertexHandle vertex_opposite_edge(MeshGeometry const& mesh,
                                  FaceHandle const& face,
                                  EdgeHandle const& edge);

void get_face_vertex_handles(MeshGeometry const& mesh,
                             FaceHandle const& face,
                             std::vector<VertexHandle>& output);

FaceHandle find_face_containing(MeshGeometry const& mesh, std::vector<VertexHandle> const& required_vertices);


#endif
