#ifndef __CrackTriangleMesh_h
#define __CrackTriangleMesh_h

#include "CrackDataTypes.h"

#include <Eigen/Core>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <boost/heap/priority_queue.hpp>
#include <set>
#include <vector>

typedef OpenMesh::VPropHandleT<bool>            VertexBooleanProperty;
typedef OpenMesh::VPropHandleT<Eigen::Vector2f> VertexVector2Property;
typedef OpenMesh::VPropHandleT<Eigen::Vector3f> VertexVector3Property;
typedef OpenMesh::VPropHandleT<Eigen::Matrix3f> VertexMatrix3Property;
typedef OpenMesh::VPropHandleT<Eigen::Matrix2f> VertexMatrix2Property;
typedef OpenMesh::VPropHandleT<float>           VertexFloatProperty;

typedef OpenMesh::FPropHandleT<Eigen::Matrix2f> FaceMatrix2Property;
typedef OpenMesh::FPropHandleT<Eigen::Matrix3f> FaceMatrix3Property;
typedef OpenMesh::FPropHandleT<float>           FaceFloatProperty;
typedef OpenMesh::FPropHandleT<bool>            FaceBooleanProperty;

typedef OpenMesh::EPropHandleT<bool>            EdgeBooleanProperty;
//typedef OpenMesh::HPropHandleT<bool>            HalfedgeBooleanProperty;

class CrackTriangleMesh
{

public:
  typedef MeshGeometry::EdgeIter      edge_iterator;
  typedef MeshGeometry::FaceIter      face_iterator;
  typedef MeshGeometry::ConstFaceIter const_face_iterator;
  typedef MeshGeometry::HalfedgeIter  halfedge_iterator;
  typedef MeshGeometry::VertexIter    vertex_iterator;
  typedef MeshGeometry::ConstVertexIter const_vertex_iterator;

  CrackTriangleMesh();
  CrackTriangleMesh(MeshGeometry const& initial_geometry);

  virtual ~CrackTriangleMesh() { }

  void set_initial_geometry(MeshGeometry const& initial_geometry);
  void set_material_toughness(float how_tough);
  void add_random_contraction(float amount);
  void add_uniform_contraction(float amount);

  void advance_simulation(float how_long);

  void set_verbosity(int how_much);
  int verbosity() const;

  int current_iteration() const { return this->CurrentIteration; }

  // Initialize the stress field using the contents of a 2D array of
  // floating-point numbers.  The value at each point will be used as
  // the strength of a uniform contraction added to each face in the
  // mesh.  The image will be sampled at coordinates corresponding to
  // the center of the face.  We will assume that the image exactly
  // covers the bounding box of the simulation mesh.
  void initialize_stress_field(DynamicArray const& initial_values);

  // Convenience method: look up a vertex position.  Cosmetic position
  // may get displaced from its original location.  Simulation
  // position is the quasi-static version.
  Vector3f material_vertex_cosmetic_position(VertexHandle const& vertex) const;
  Vector3f crack_vertex_cosmetic_position(VertexHandle const& vertex) const;
  Vector3f vertex_simulation_position(VertexHandle const& vertex) const;

  // Get handles to the simulation mesh or the crack mesh.  Please
  // don't change either of them.  It will make life so much more
  // painful for me.  In fact, I would like for you to use them only
  // to look things up.
  MeshGeometry const& material_mesh() const;
  MeshGeometry const& crack_mesh() const;

  // Walk around the boundary edges of the mesh and set them all to be
  // fixed -- this is meant for use during setup
  void fix_boundary_edges();


  void debug_print_mesh_topology();
  void debug_print_vertices();
  void debug_print_vertex(VertexHandle const& v) const;
  void debug_print_halfedges();
  void debug_print_halfedge(HalfedgeHandle const& he) const;
  void debug_print_edge(EdgeHandle const& e) const;
  void debug_print_edges();
  void debug_print_faces();

  // Print stats about the force on a vertex
  void debug_print_vertex_properties(VertexHandle const& vertex);
  void debug_print_face_properties(FaceHandle const& face);
  void debug_print_mesh_properties();
  void debug_print_separation_eigenvalues();
  void debug_set_single_triangle_mesh();
  void debug_set_rectangular_triangle_mesh(int rows, int cols, bool add_high_stress_to_corner=true);
  void debug_write_separation_image(int rows, int cols, const char* filename, float maxval);

  // Has the mesh topology changed since the last time we checked?
  bool topology_has_changed() const;
  void acknowledge_topology_change();

  // Get the total separation stress from the last iteration.
  // Measured in force per unit area.
  float total_separation_stress() const { return this->TotalSeparationStress; }
  
protected:
  class FailureRecord
  {
  public:
    FailureRecord(MeshGeometry::VertexHandle where, float stress, Eigen::Vector3f const& crack_plane_normal)
      : CrackPlaneNormal(crack_plane_normal)
      , Location(where)
      , SeparationEigenvalue(stress)
      { }

    bool operator<(FailureRecord const& other) const
      {
        return (this->SeparationEigenvalue < other.SeparationEigenvalue);
      }

    MeshGeometry::VertexHandle Location;
    float SeparationEigenvalue;
    Eigen::Vector3f CrackPlaneNormal;
  };

  class CrackVertex
  {
  public:
    CrackVertex(
      FaceHandle const& face,
      EdgeHandle const& edge,
      VertexHandle const& origin_vertex,
      Vector3f const& crack_vertex_position,
      Vector3f const& crack_normal
      )
      : CrackVertexPosition(crack_vertex_position)
      , CrackPlaneNormal(crack_normal)
      , Edge(edge)
      , Face(face)
      , OriginVertex(origin_vertex)
      , VertexIsNew(true)
      { }

    Vector3f CrackVertexPosition;
    Vector3f CrackPlaneNormal;
    EdgeHandle Edge;
    VertexHandle CrackVertexHandle;
    FaceHandle Face;
    VertexHandle OriginVertex;
    bool VertexIsNew;
  };

protected:
  typedef boost::heap::priority_queue<FailureRecord> FailureQueue;

  // The simulation will run on this mesh
  MeshGeometry MaterialMesh;

  // Explicit representation of the space between the cracks
  MeshGeometry CrackMesh;

  // How long does the current timestep last?
  float CurrentTimeStep;
  float RelaxTimeStep;

  // How long has the simulation been going on?
  float SimulationTime;
  int CurrentIteration;

  // How fine should the "relax" steps be?
  int NumRelaxStepsPerIteration;

  // Vertices where the material is over-stressed
  FailureQueue FailingVertices;

  // How close can we get to an existing edge before we need to snap a
  // crack edge instead of introducing a new vertex?
  float CrackEdgeSnapThreshold;
  float InteriorEdgeSnapThreshold;

  // Homogeneity constant.  The closer it is to 1, the longer and
  // smoother cracks should be.  Must be between 0 and 1.
  float MaterialHomogeneity;

  // How tough is the material?  (this may be overridden per vertex)
  float Toughness;

  float TotalSeparationStress;
  
  VertexFloatProperty   LargestSeparationEigenvalue;
  VertexVector3Property LatestVertexDisplacement;
  VertexFloatProperty   MaterialToughness;
  VertexVector3Property MaterialVertexSimulationPosition;
  VertexMatrix3Property SeparationTensor;
  VertexMatrix3Property SeparationTensorResidual;
  VertexVector3Property TotalCompressiveForce;
  VertexVector3Property TotalTensileForce;
  VertexBooleanProperty VertexBelongsToIsolatedFace;
  VertexBooleanProperty VertexIsOnCrackBoundary;
  VertexBooleanProperty VertexIsCrackTip;
  VertexBooleanProperty VertexIsFixed;

  FaceMatrix3Property BarycentricBasisMatrix;
  FaceFloatProperty   FaceArea;
  FaceMatrix2Property StressTensor;
  FaceMatrix2Property StressTensorDerivatives[3][3];
  FaceBooleanProperty FaceIsIsolated;
  
  EdgeBooleanProperty  EdgeIsFixed;

  bool Verbose;
  int Verbosity;

  // This flag will be set when we have modified the mesh itself
  // instead of just pushing vertices around
  bool TopologyHasChanged;

  // Convenience method: add a face to the mesh and set its properties
  FaceHandle add_face(std::vector<VertexHandle> const& vertices);

  // Convenience method: copy a face from the material mesh to the crack mesh
  FaceHandle add_face_to_crack_mesh(FaceHandle const& face);

  // Convenience method: add a vertex to the mesh and set its
  // properties
  VertexHandle add_vertex(MeshGeometry::Point const& position);

  // Reduce the stress in the separation tensor after a new crack has
  // formed
  void add_residual_separation_tensor(VertexHandle const& failed_vertex,
                                      VertexHandle const& crack_tip,
                                      Vector3f const& crack_plane_normal);

  // Skip over a gap in a vertex's 1-ring.
  HalfedgeHandle advance_halfedge_past_gap(HalfedgeHandle const& he) const;

  // Move a halfedge counterclockwise around its to-vertex to the next
  // face.
  HalfedgeHandle advance_halfedge_to_next_face(HalfedgeHandle const& he) const;
  
  // Zero out the forces on a particular vertex
  void clear_forces_on_vertex(VertexHandle const& v_handle);

  // Zero out the separation tensor on each node (will happen at start of
  // iteration)
  void clear_separation_tensors();

  // Loop over the vertices and update all the virtual displacements
  void compute_all_vertex_displacements(float timestep);

  // Compute the barycentric basis matrices used to transfer points and
  // vectors between triangle-local and world space - used during setup
  void compute_barycentric_basis();
  void compute_barycentric_basis(FaceHandle const& face);

  // Compute the area of each face in the mesh - used during setup
  void compute_face_areas();
  void compute_face_area(FaceHandle const& face);

  // Compute the force on a given vertex due to the stress tensor at
  // one of its incident faces
  Vector3f compute_force_on_vertex(
    VertexHandle const& vertex,
    int vertex_id,
    std::vector<Vector3f> const& vertex_coordinates,
    double face_area,
    Matrix3f const& barycentric_basis,
    Matrix2f const& stress_tensor,
    bool is_compressive) const;

  // Compute all the stress tensor derivatives.
  void compute_stress_tensor_derivatives();

  // Compute the stress tensor derivatives for one particular face.
  void compute_stress_tensor_derivatives(FaceHandle const& face);

  // Compute the derivative of the stress tensor for a given face with
  // respect to a given vertex
  void compute_stress_tensor_derivatives_at_vertex(
    FaceHandle const& face,
    int vertex_id,
    std::vector<Vector3f> const& vertex_positions,
    Matrix3f const& barycentric_basis);

  // Loop over all the vertices and compute how much each one should
  // move
  void compute_all_vertex_displacements();

  // Compute how much a vertex should move given the total force
  // incident upon it
  Eigen::Vector3f compute_virtual_displacement(VertexHandle const& vertex, float timestep) const;

  // Copy simulation face properties from one face to another
  void copy_face_properties(FaceHandle const& from, FaceHandle const& to);
  
  // Copy vertex properties from one vertex to another
  void copy_vertex_properties(VertexHandle const& from, VertexHandle const& to);

  // Search the mesh for perforation corners and get rid of them
  void disconnect_perforation_corners();

  // Disconnect one particular perforation corner
  void disconnect_perforation_corner(HalfedgePair const& here);
  
  // Analyze the stress tensor for a given face and contribute to the
  // separation tensor at each of that face's vertices
  void distribute_stress_from_face(FaceHandle const& face);

  // Loop over the entire mesh and distribute stresses from each face
  // to form the separation tensor at each vertex
  void distribute_stress_into_separation_tensors();

  // Make an exact copy of an existing vertex
  virtual VertexHandle duplicate_vertex(VertexHandle const& which_vertex);

  // Check whether an edge lies on a crack boundary
  bool edge_is_crack_boundary(EdgeHandle const& edge) const;
  bool edge_is_crack_boundary(HalfedgeHandle const& he) const;
  bool edge_is_crack_boundary(VertexHandle const& v1, VertexHandle const& v2) const;

  // Place crack vertices in the mesh, including sanity checks and
  // property updates
  void emplace_new_crack_vertex(CrackVertex& new_vertex);
  void emplace_crack_vertices(std::vector<CrackVertex>& new_vertices);

  // Make note of a vertex under too much stress - a failure may occur
  // here
  void enqueue_new_failure(VertexHandle const& vertex,
                           EigenPair3 const& stress_info);

  // Identify the places where we need to introduce vertices for a new
  // crack.  This does not do any sanity checking -- just find the
  // intersections between the crack plane and the edge.
  int find_crack_vertex_positions(VertexHandle const& center,
                                  Vector3f const& crack_normal,
                                  std::vector<CrackVertex>& results) const;


  // Find the vertices in the mesh that should be disconnected.
  void find_perforation_corners(std::vector<HalfedgePair>& results) const;
  
  // Given two faces that share an edge, find the vertices that do not
  // belong to that shared edge.
  std::pair<VertexHandle, VertexHandle> find_unique_vertices(FaceHandle const& face1, FaceHandle const& face2) const;

  // Part of the separation tensor can only be computed once we know
  // the total force on a vertex.  We do that here.
  void finish_computing_separation_tensor(VertexHandle const& vertex);

  // Iterate over the faces that get created when a crack vertex is
  // placed and make sure they get appropriate properties
  void fix_faces_after_split(VertexHandle const& crack_center_vertex,
                             VertexHandle const& near_face_vertex,
                             VertexHandle const& far_face_vertex,
                             Matrix2f const& near_stress_tensor,
                             Matrix2f const& far_stress_tensor,
                             bool far_face_exists);

  // Retrieve the (quasi-static) world-space positions for the
  // vertices surrounding the given face
  void get_face_vertex_positions(FaceHandle const& face,
                                 std::vector<Vector3f>& output) const;


  // Identify nodes where the material should fail
  void enqueue_failing_vertices();

  // Find the first half-edge around a vertex that is either a crack
  // edge or is opposite a boundary edge.
  std::size_t find_first_boundary_halfedge(VertexHandle const& center_vertex, std::set<VertexHandle> const& crack_tips) const;

  // Check a half-edge to find out whether it goes between a crack tip
  // and crack center or whether it is a boundary edge.
  bool halfedge_is_crack_or_boundary(HalfedgeHandle const& he, std::set<VertexHandle> const& crack_tips) const;

  // Check a half-edge to find out whether it goes between a crack tip
  // and crack center or whether its partner is a boundary edge.
  bool halfedge_is_crack_or_opposite_boundary(HalfedgeHandle const& he, std::set<VertexHandle> const& crack_tips) const;

  // Find and flag faces that have no neighbors
  void identify_disconnected_faces();
  
  // Set sane defaults on the various properties when we add an
  // element
  void initialize_edge_property_values(EdgeHandle const& edge);
  void initialize_face_property_values(FaceHandle const& face);
  void initialize_vertex_property_values(VertexHandle const& vertex);

  // Set the properties on a face that just got split by a crack
  void initialize_face_fragment(FaceHandle const& face, Matrix2f const& stress_tensor);

  // One-time startup
  void initialize_mesh_properties();
  void initialize_mesh_property_values();

  // Initialize property values for all the faces surrounding a given
  // vertex.  This will wipe out any values that are already there.
  void initialize_neighboring_faces(VertexHandle const& center_vertex);

  // Introduce new cracks at failed nodes
  void introduce_new_cracks();

  // Flag a vertex as a crack tip (or not)
  void mark_crack_tip(VertexHandle const& v, bool is_crack);

  // Flag a vertex as part of a carck (or not)
  void mark_vertex_on_crack_boundary(VertexHandle const& v, bool is_crack);

  // Move the vertices in the renderable mesh to simulate cracks
  // opening up
  void move_cosmetic_vertex(VertexHandle const& vertex,
                            Vector3f const& displacement);

  // Once we have a new mesh, set all the property values for the
  // start of the simulation
  void populate_mesh_properties();

  // Take several tiny steps to relax the stress field
  void relax_stress_field();

  // Delete faces belonging to the two vertices on either side of a crack
  void remove_crack_faces(std::vector<VertexHandle> const& required_vertices);

  // Convenient accessor method
  Matrix3f separation_tensor(VertexHandle const& v) const;

  // Set a particular edge to be fixed (or not) in space
  void set_edge_fixed(EdgeHandle const& edge, bool is_fixed);

  // Restore the stress tensors in the neighborhood around a vertex.
  // Requires that you have previously called
  // save_stress_tensors_around_vertex for that neighborhood.
  void restore_stress_tensors_around_vertex(VertexHandle const& v,
                                            std::map<EdgeHandle, Matrix2f> const& archive);

  void sanity_check_faces() const;

  // Save the stress tensors for all the faces around a vertex.  Index
  // them by the edge opposite that vertex -- that should uniquely
  // identify the face in the neighborhood.
  void save_stress_tensors_around_vertex(VertexHandle const& v,
                                         std::map<EdgeHandle, Matrix2f>& results) const;


  // Examine a proposed crack edge.  If it is too close to an existing
  // edge (internal or boundary), snap the edge to that existing one
  // and note that we do not need to create a new vertex.
  void snap_crack_to_nearby_edges(CrackVertex& input) const;

  // Divide the vertex where a crack started into two vertices and
  // make a gap in the material
  void split_crack_center(VertexHandle const& vertex_to_split,
                          std::vector<CrackVertex> const& crack_vertices);

  // Utility method: split a crack center that is on the inside of the
  // mesh.
  VertexHandle split_crack_center_interior(VertexHandle const& vertex_to_split,
                                           std::vector<CrackVertex> const& crack_vertices);

  // Utility method: split a crack center that is on the boundary of
  // the mesh.
  VertexHandle split_crack_center_boundary(VertexHandle const& vertex_to_split,
                                           std::vector<CrackVertex> const& crack_vertices);

  // Compute and emplace the new vertices for this crack, then update
  // the stress and separation tensors to account for the relief of
  // stress
  void start_new_crack(FailureRecord const& failure);

  // Store three 2-vectors in the columns of a 3x3 matrix and save it
  // in the properties for a face - used during setup
  void store_in_plane_vertex_positions(FaceHandle const& face,
                                       std::vector<Vector2f> const& positions);

  // Check the separation tensor at a vertex to see if the stress on
  // the material exceeds its failure threshold (i.e. the material
  // should crack)
  bool stress_exceeds_failure_threshold(MeshGeometry::VertexHandle const& vertex) const;

  // Retrieve the stress tensor at a given face in the mesh
  Matrix2f const& stress_tensor(FaceHandle const& face) const;

  // Compute the total force on a vertex
  //
  // NOTE: This is only valid after the separation tensor has been
  // computed
  Eigen::Vector3f total_force_on_vertex(VertexHandle const& vertex) const;

  // Move vertex positions from the user-supplied geometry mesh to the
  // internal simulation mesh - used during setup
  void transfer_vertices_to_simulation();

  // Update the stress tensor at a single face
  void update_stress_tensor_at_face(FaceHandle const& face);

  // Update the stress tensor on each face
  void update_stress_tensors();

  // Convenience method: look up whether a vertex is on a crack boundary
  bool vertex_is_on_crack_boundary(VertexHandle const& v) const;


  // Debug methods below here
  std::string face_coordinates_as_string(FaceHandle const& f) const;

};

#endif
