#include "CrackSimulationMath.h"
#include <iostream>

void append_one(std::vector<Vector2f> const& input,
                std::vector<Vector3f>& output)
{
  for (int i = 0; i < 3; ++i)
    {
    for (int j = 0; j < 2; ++j)
      {
      output[i](j) = input[i](j);
      }
    output[i](2) = 1;
    }
}

int main(int argc, char* argv[])
{
  std::vector<Vector3f> corners(3);
  corners[0] = Vector3f(1, 0, 0);
  corners[1] = Vector3f(0, 5, 0);
  corners[2] = Vector3f(10, 16, 0);

  std::vector<Vector2f> points_in_local_space;
  project_triangle_vertices_to_local_space(corners, points_in_local_space);

  std::vector<Vector3f> extended_local_points(3);;
  append_one(points_in_local_space, extended_local_points);

  Matrix3f bary_basis = construct_barycentric_basis(corners);

  std::cout << "Barycentric basis:\n" << bary_basis << "\n";
  std::cout << "Vertices times basis:\n";
  for (int i = 0; i < 3; ++i)
    {
    std::cout << "(" << extended_local_points[i].transpose() << ") -> ("
              << (bary_basis * extended_local_points[i]).transpose() << ")\n";
    }


  // try a more intuitively interpretable example
  corners[0] = Vector3f(10, 10, 10);
  corners[1] = Vector3f(0, 10, 0);
  corners[2] = Vector3f(10, 0, 0);

  return 0;
}
