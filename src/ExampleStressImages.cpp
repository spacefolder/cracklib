#include "ExampleStressImages.h"
#include <algorithm>

void make_black_image(DynamicArray& image, int height, int width)
{
  // The array is indexed in row-major order
  image = DynamicArray::Zero(height, width);
}

// ----------------------------------------------------------------------

void make_white_image(DynamicArray& image, int height, int width)
{
  // The array is indexed in row-major order
  image = DynamicArray::Ones(height, width);
}

// ----------------------------------------------------------------------

void invert_image(DynamicArray& image)
{
  for (int r = 0; r < image.rows(); ++r)
    {
    for (int c = 0; c < image.cols(); ++c)
      {
      image(r, c) = 1 - image(c, r);
      }
    }
}

// ----------------------------------------------------------------------

void add_to_image(DynamicArray& image, float how_much)
{
  for (int r = 0; r < image.rows(); ++r)
    {
    for (int c = 0; c < image.cols(); ++c)
      {
      image(r, c) += how_much;
      }
    }
}

// ----------------------------------------------------------------------

void inner_square(DynamicArray& image, float fill_fraction, float fill_value)
{
  float dark_fraction = 1 - fill_fraction;

  int first_fill_column = static_cast<int>(dark_fraction * 0.5 * image.cols());
  int last_fill_column = static_cast<int>(image.cols() * (1 - 0.5 * dark_fraction));

  int first_fill_row = static_cast<int>(dark_fraction * 0.5 * image.rows());
  int last_fill_row = static_cast<int>(image.rows() * (1 - 0.5 * dark_fraction));

  // Clip to the image borders
  int i_rows = image.rows();
  int i_cols = image.cols();
  first_fill_column = std::max(0, std::min(first_fill_column, i_cols - 1));
  last_fill_column = std::max(0, std::min(last_fill_column, i_cols - 1));

  first_fill_row = std::max(0, std::min(first_fill_row, i_rows - 1));
  last_fill_row = std::max(0, std::min(last_fill_row, i_rows - 1));

  for (int r = first_fill_row; r <= last_fill_row; ++r)
    {
    for (int c = first_fill_column; c <= last_fill_column; ++c)
      {
      image(r, c) = fill_value;
      }
    }
}

// ----------------------------------------------------------------------

int stress_image_width(DynamicArray const& image)
{
  return image.rows();
}

int stress_image_height(DynamicArray const& image)
{
  return image.cols();
}

// ----------------------------------------------------------------------

float& stress_image_pixel(DynamicArray& image, int w, int h)
{
  return image(h, w);
}
