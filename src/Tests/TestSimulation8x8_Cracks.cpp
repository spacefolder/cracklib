/*
 * The one and only purpose of this test is to see if we can compile
 * our Eigen + OpenMesh hybrid class.
 */

#include <iostream>

#include <CrackTriangleMesh.h>

int main(int argc, char* argv[])
{
  CrackTriangleMesh mesh;
  mesh.set_material_toughness(2);

  mesh.debug_set_rectangular_triangle_mesh(4, 4);
  mesh.set_verbosity(5);
  for (int i = 0; i < 1000; ++i)
    {
    mesh.advance_simulation(0.01);
    mesh.add_uniform_contraction(0.02);
    mesh.add_random_contraction(0.0002);
    }
//  mesh.advance_simulation(0.01, true);

  std::cout << "Success.\n";
  return 0;
}
